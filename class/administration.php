<?php
/**
 * A class that contains code to implement different module leader, theme leader and suppervisor tasks
 *
 * @author Plamen Kolev <p.kolev@newcastle.ac.uk
 * @copyright 2016 Plamen Kolev
 *
 */
/**
 * Class for the /theme page
 */
    class Administration extends Siteaction
    {
/**
 * Handle administration operations /administrator/xxxx
 *
 * @param object $context The context object for the site
 * @return mixed|string $String   A template name
 */
        public function handle($context)
        {
            # determins restfulness
            $rest = $context->rest();
            switch ($rest[0])
            {
                case 'theme':
                    $context->mustbemoduleleader();
                    return $this->index($context);
                break;

                case 'assign_supervisor':
                    return $this->assign_supervisor($context);
                break;

                case 'remove_supervisor':
                    return $this->remove_supervisor($context);

                case 'supervisor_home':
                    $context->mustbesupervisor();
                    return $this->supervisor_home($context);
                    break;
                case 'manage_roles':
                    $context->mustbemoduleleader();
                    return 'administration/manage_roles.twig';
                    break;
                default:
                    return 'error/404.twig';

            }
        }


/**
 * Removes a supervisor from a student, useful when supervisor role needs to be revoked
 * @param $context
 */
        public function remove_supervisor($context)
        {
            $student_id = $context->rest()[1];

            $student = R::load('user', $student_id);
//            dd($student_id);
            if (!$student->id)
            {
                $context->httpbadrequest();
            }
            $student->supervisor = null;
            R::store($student);
            header('Location: ' . Config::BASEDNAME . '/administration/assign_supervisor');
            exit;
        }

/**
 * CRUD home for this controller
 * @param   $context      Context
 * @return string $String    Twig template
 */

        public function index($context)
        {
            // if submiting a form, process it here
            if ($_POST)
            {
                $formdata = $context->formdata();
                $required_data = $formdata->haspost('user_id') && $formdata->haspost('choice');
                if (!$required_data)
                {
                    header('HTTP/1.0 400');
                    echo 'Malformed request!';
                    exit;
                }
                $user_id = $formdata->post('user_id');
                $theme_id = $formdata->post('choice');

                $user = R::load('user', $user_id);
                $theme_choice = R::load('theme', $theme_id);

                if (! $user->id || ! $theme_choice->id)
                {
                    $context->local()->addval('warnmessage', ["We could not find the user or the choices associated with him/her !"]);
                    return 'theme/index.twig';
                } 
                
                $choice = R::findOne('themechoice',
                 'confirmed = 0 and user_id = :user_id',
                    array(':user_id'=>$user->id) 
                );

                if ($choice && $choice->id)
                {
                    $choice->first = $theme_choice;
                    $choice->confirmed = true;
                    Mailman::sendmail(
                        $user,
                        "Theme choice has been confirmed",
                        "The theme you have been assigned to is " . $theme_choice->title
                    );
                    R::store($choice);
                } 
                else 
                {
                    $context->web()->bad( "We could not find a theme choice for the user " + $user->login );
                }
            }

            $themes = R::findAll('theme');
            $choices = R::findAll('themechoice');
            $context->local()->addval('themes', $themes);
            $context->local()->addval('choices', $choices);
            return 'administration/index.twig';
        }

/**
 * Page displaying all students associated with a supervisor
 *
 * @param $context      Framework Context
 * @return string $String      twig template
 *
 */
        public function supervisor_home($context)
        {
            $my_students = $context->user()->supervisees();
            $context->local()->addval('students', $my_students);
            return 'administration/supervisor_home.twig';
        }

/**
 * The page where moduleleader and theme leader can assign a supervisor to each student
 * @param $context
 * @return string
 */
        public function assign_supervisor($context)
        {
            $context->mustbethemeormoduleleader();
            if ($_POST)
            {
                $formdata = $fdt = $context->formdata();
                $hasdata = $formdata->haspost('user_id') && $formdata->haspost('supervisor');
                if (!$hasdata)
                {
                    header('HTTP/1.0 400');
                    echo 'Malformed request!';
                    exit;
                }
                $user_id = $formdata->post('user_id');
                $supervisor_id = $formdata->post('supervisor');

                $user = R::load('user', $user_id);
                $supervisor = R::load('user', $supervisor_id);

                if (! $user->id || ! $supervisor->id)
                {
                    $context->local()->addval('warnmessage', ["We could not find the user or the supervisor associated with her/him !"]);
                    return 'administration/assign_supervisor.twig';
                } 
                if (! $supervisor->hasrole('Site', 'Supervisor') )
                {
                    $context->local()->addval('warnmessage', ["Invalid supervisor"]);
                    return 'administration/assign_supervisor.twig';
                }

                $user->supervisor = $supervisor;
                Mailman::sendmail(
                    $user,
                    "Your supervisor has been assigned",
                    "A supervisor has been assigned to you, arrange your first meeting by writing an email at " . $supervisor->email
                );

                Mailman::sendmail(
                    $supervisor,
                    "New student has been assigned for your supervision",
                    "Student with email " . $user->email . " should be in touch with you soon"
                );

                R::store($user);
            }

            # if a theme leader is logged in, only give themes that he owns
            $students;
            if ($context->user()->isthemeleader())
            {
                $students = $context->user()->studentswithmytheme();
            # for module leader, return all themes
            }
            else
            {
                $students = Model_User::students_with_confirmed_themes();    
            }
            
            $supervisors = Model_User::supervisors();
            $context->local()->addval('students', $students);
            $context->local()->addval('supervisors', $supervisors);
            return 'administration/assign_supervisor.twig';
        }

    }
?>
