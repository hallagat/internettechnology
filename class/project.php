<?php
/**
 * A class that contains code to implement the project controller logic
 *
 * @author Plamen Kolev <p.kolev@newcastle.ac.uk
 * @copyright 2016-2017 Newcastle University
 *
 */

/**
 * Class for the /project page
 */
class Project extends Siteaction
{
    /**
     * Handle theme operations /project/xxxx
     *
     * @param object $context
     * @return mixed|string $string    A template name
     * @internal param $object $context    The context object for the site
     */
    public function handle($context)
    {
        # determine restfulness
//        let only module leader, themeleader owning the project or supervisor associated with a project here

        $rest = $context->rest();

        switch ($rest[0])
        {
            case 'create':
                return $this->create($context, $rest[1]);

            case 'edit':
                return $this->edit($context, $rest[1]);
            default:
                return 'error/404.twig';
        }
    }

/**
 * Module leader or theme leader owning this theme can create projects for it
 * @param $theme_id
 * @return $string
 */
    public function create($context, $theme_id = -1)
    {
        $canaccess = $context->user()->ismoduleleader() || $context->user()->isthemeleader($theme_id);
        if (!$canaccess)
        {
            $context->httpforbidden();
        }
        // serialise it
        $theme_id = intval($theme_id);
        $theme = R::load('theme', $theme_id);
        if (!$context->user()->ownstheme($theme))
        {
            $context->httpforbidden();
        }
        if (!$theme->id)
        {
            $context->httpnotfound('Theme not found');
        }

        if ($_POST)
        {

            if (Model_Project::create_or_edit($theme_id))
            {
                header('Location: ' . Config::BASEDNAME . '/themes');
                exit;
            }
        }

        $supervisors = Model_User::supervisors();
        $context->local()->addval('theme', $theme);
        $context->local()->addval('supervisors', $supervisors);
        return 'project/create_or_edit.twig';

    }

/**
 * Module leader or theme leader owning this theme can edit projects for it
 */
    public function edit($context, $project_id = -1)
    {


        $project = R::load('project', intval($project_id));
        $canaccess = $context->user()->ismoduleleader() || $context->user()->ownstheme($project->theme) || $context->user()->id == $project->supervisor_id;
        if (!$canaccess)
        {
            $context->httpforbidden();
        }

        if (!$project->id)
        {
            $context->httpnotfound('Project not found');
        }
        if($_POST)
        {
            if(Model_Project::create_or_edit($project_id, 'edit'))
            {
                header('Location: ' . Config::BASEDNAME . '/themes');
                exit;
            }
        }
        $supervisors = Model_User::supervisors();
        $context->local()->addval('project', $project);
        $context->local()->addval('supervisors', $supervisors);
        return 'project/create_or_edit.twig';

    }
}