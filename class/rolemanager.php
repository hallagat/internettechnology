<?php
/**
 * A class that contains code to implement managing  supervisors and theme leaders
 *
 * @author Plamen Kolev <p.kolev@newcastle.ac.uk
 * @copyright 2016 Plamen Kolev
 *
 */
/**
 * Class for the /theme page
 */
    class Rolemanager extends Siteaction
    {
        /**
         * Handle administration operations /administrator/xxxx
         *
         * @param object $context The context object for the site
         * @return mixed|string $String   A template name
         */
        public function handle($context)
        {

            # determins restfulness
            $context->mustbemoduleleader();
            $rest = $context->rest();

            switch ($rest[0]) {
                case 'supervisors':
                    return $this->supervisors($context);


                case 'add_supervisor':
                    return $this->add_supervisor($context);


                default:
                    return 'error/404.twig';
            }
        }

/**
 * Function used by module leaders to mark users as theme leaders and supervisors
 * @param $context
 */
        public function supervisors($context)
        {
            $rest = $context->rest();
            // if the post request contains the id, it is a delete request
            if ($_POST) {
                if (sizeof($rest) > 1) {
                    $this->remove_supervisor($context);
                }
            }

            $theme_leaders = Model_User::themeleaders();
            $supervisors = Model_User::supervisors();

            $context->local()->addval('themeleaders', $theme_leaders);
            $context->local()->addval('supervisors', $supervisors);
            return "rolemanager/supervisor.twig";
        }

/**
 * Adds a new supervisor from the available users, any user can be assigned, old supervisors will just restate their roles
 * @param $context
 */
        public function add_supervisor($context)
        {
            if ($_POST)
            {
                $formdata = $context->formdata();
                if (! $formdata->haspost('supervisor'))
                {
                    $context->httpbadrequest();
                }
                $new_supervisor_id = $formdata->post('supervisor');
                $new_supervisor = R::load('user', $new_supervisor_id);
                if (!$new_supervisor->id)
                {
                    $context->local()->addval('warnmessage', ["Unable to find a user with that id in the system !"]);
                }
                else
                {
                    $new_supervisor->addrole('Site', 'Supervisor', '','');
                    header('Location: ' . Config::BASEDNAME . '/rolemanager/supervisors');
                    exit;
                }
            }
            $users = R::find('user');
            $context->local()->addval('users', $users);
            return 'rolemanager/add_supervisor.twig';
        }

/**
 * Removes the user's ability to be a supervisor
 * No need to check for permissions, as this function is redirected from manage rules, which enforces module leader privileges
 * @param $context
 */
        public function remove_supervisor($context)
        {

            $formdata = $context->formdata();
            if ($formdata->haspost('_METHOD')) {
                // grap the id of the supervisor that needs to be deleted
                // then check if that supervisor has been assigned
                // cannot delete assigned supervisors, return error if that is the case
                $id = $context->rest()[1];
                $rows = R::getAll('
                            SELECT count(id) FROM webproject.user where supervisor_id=?;
                        ', array($id));

                // positive integer means that the supervisor is active, thus cannot remove privileges
                if (intval($rows[0]['count(id)'])) {
                    $context->local()->message('warnmessage', ["Cannot revoke the supervisor role of an active supervisor!"]);
                    //                        return 'index.twig';
                } else {
                    // if the supervisor is innactive, grab its object and revoke the role
                    $user = R::load('user', $id);
                    $user->delrole('Site', 'Supervisor');
                }

            }
        }
    }
