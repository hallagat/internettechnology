<?php
/**
 * A class that contains code to implement the theme display page functionality
 *
 * @author Plamen Kolev <p.kolev@newcastle.ac.uk
 * @copyright 2016-2017 Newcastle University
 *
 */
/**
 * Class for the /theme page
 */
    class Theme extends Siteaction
    {
/**
 * Handle theme operations /theme/xxxx
 *
 * @param object $context $context    The context object for the site
 * @return mixed|string $string    A function corresponding to the request
 *
 */
        public function handle($context)
        {
            # determins restfulness
            $rest = $context->rest();
            switch ($rest[0])
            {
                case '':
                    return $this->index($context);
                break;

                case 'edit':
                    return $this->edit($context, $rest[1]);

                case 'choice':
                    return $this->choice($context);
                default:
                    return 'error/404.twig';

            }
        }

/**
 * This function allows a person to view themes and projects, also allows owners to hide and edit them
 *
 * @param   $context  Framework context
 * @return string $string   template name
 *
 */
        public function index($context)
        {
            $themes;
            // module leader can see all themes, including the hidden ones
            if ($context->user()->ismoduleleader())
            {
                $themes = R::findAll( 'theme' );
            } 

            // theme leader can see only themes that he/she hid
            elseif ($context->user()->isthemeleader()) {
                $themes =   R::findAll( 'theme', 'visible=1' );
                # now add themes that belong to that theme leader and are hidden
                $myhiddenthemes = R::findAll( 'theme', 'visible=0 and leader_id=?', array($context->user()->id) );
                $themes =array_merge($myhiddenthemes, $themes);
            } 
            else 
            {
                $themes =   R::findAll( 'theme', 'visible=1' );
            }
            
            $context->local()->addval('themes', $themes);

            return 'theme/index.twig';
        }

/**
 * This function gets triggered when a user visits or submits a theme preference
 * 
 * @param  $contextObject  Framework context
 * @return $string   template name
 *
 */
        public function choice($context)
        {

            # find if the user has chosen their modules, return if so
            if (! $context->hasuser())
            {
                $context->local()->addval('warnmessage', ["You must be logged in to be here"]);
                return 'theme/index.twig';   
            } 
            else if (! $context->user()->canchosetheme())
            {
                $context->local()->addval('warnmessage', ["You have already selected a module themes or you are not a student"]);
                return 'theme/index.twig';
            }

            if ($_POST)
            {
                $choice = R::dispense( 'themechoice' );
                $choice->create();

                return 'theme/index.twig';
            }
            $themes = R::findAll('theme', 'visible=1');
            $context->local()->addval('themes', $themes);
            return 'theme/choice.twig';
        }

/**
 * This function gets triggered when a module leader or theme leader edits a theme
 *
 * @param   $context  Framework context
 * @param int $id
 * @return string $string   template name
 *
 */

        public function edit($context, $id=-1)
        {
            $theme = R::load( 'theme', $id );
            // do not allow anyone to edit the theme except the module leader and the theme leader owner
            if ($context->user() && $context->user()->ownstheme($theme) )
            {
                if (!$theme)
                {
                    return 'error/404.twig';
                }

                if ($_POST)
                {
                    $theme->edit();
                }
                
                $context->local()->addval('theme', $theme);
                return 'theme/edit.twig';

            } else {
                header('HTTP/1.0 403 Forbidden');
                echo 'Unauthorised!';
                exit;    
            }
            
        }

    }