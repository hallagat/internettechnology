<?php
/**
 * A class that contains code to implement the api interface
 *
 * @author Plamen Kolev <p.kolev@newcastle.ac.uk
 * @copyright 2016-2017 Newcastle University
 *
 */
/**
 * Class for the /theme page
 */
    class Api extends Siteaction
    {
        /**
         * Handle theme operations /api/xxxx
         *
         * @param object $context $context    The context object for the site
         * @return mixed|string $string    A function corresponding to the request
         *
         */
        public function handle($context)
        {
            # determins restfulness
            $rest = $context->rest();
            switch ($rest[0])
            {
                case 'themes':
                    return $this->themes($context);
                    break;

                case 'projects':
                    return $this->projects($context);
                    break;

                default:
                    return 'error/404.twig';

            }
        }

/**
 * Api returning json for themes
 * @param $context
 * @return string
 */
        public function themes($context)
        {
            $rest = $context->rest();
            // if it has more rest segments, assume this is the single view
            if (sizeof($rest) > 1)
            {
                $themes = R::find('theme', 'visible=1 and id=?', [$rest[1]]);
            }
            else
            {
                $themes = R::find('theme');
            }

            // do not show themes as they are, this will be a security voulnrability
            $parsed_themes = [];
            foreach ($themes as $theme)
            {
                $wrapper = new Themewrapper;
                $wrapper->id = $theme->id;
                $wrapper->title = $theme->title;
                $wrapper->description = $theme->description;
                $wrapper->theme_leader = $theme->themeleader()->email;
                array_push($parsed_themes, $wrapper);


            }
            $json_string = json_encode($parsed_themes);
            $context->web()->sendstring($json_string, 'application/javascript');
            return '';
        }

/**
 * Restful interface for projects
 * @param $context
 * @return string
 */
        public function projects($context)
        {
            $rest = $context->rest();
            // if it has more rest segments, assume this is the single view
            if (sizeof($rest) > 1)
            {
                $projects = R::find('project', 'visible=1 and id=?', [$rest[1]]);
            }
            else
            {
                $projects = R::find('project');
            }

            // do not show themes as they are, this will be a security voulnrability
            $parsed_projects = [];
            foreach ($projects as $project)
            {
                $wrapper = new Themewrapper;
                $wrapper->id = $project->id;
                $wrapper->title = $project->title;
                $wrapper->description = $project->description;
                $wrapper->theme = $project->theme->title;
                $wrapper->supervisor = $project->supervisor()->email;
                array_push($parsed_projects, $wrapper);


            }
            $json_string = json_encode($parsed_projects);
            $context->web()->sendstring($json_string, 'application/javascript');
            return '';
        }
    }

/**
 * Class Themewrapper, throwaway class that prevents users viewing information they are not supposed to
 */
    class Themewrapper{}
    class Projectwrapper{}

