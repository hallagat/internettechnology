<?php
/**
 * A wrapper for mail operations
 *
 * @author Plamen Kolev <p.kolev@newcastle.ac.uk>
 * @copyright 2016 Plamen Kolev
 *
 */
    class Mailman
    {
        use Singleton;
        
/**
 * Function used to send emails to one or more users
 * @param   $to   This argument  can be either Redbean User object, or an array of Redbean User object
 * @param   @subject    The subject line in the email
 * @param   $body       The actual body text of the email message
 */
        public static function sendmail($to, $subject, $body)
        {
            // error checking to see if the recepient user is found in our record
            if (is_array ( $to ))
            {
                foreach ($to as $user) {
                    if (!$user || ($user && !$user->id) )
                    {
                        header('HTTP/1.0 400');
                        echo 'Email address not found!';
                        exit;
                    }
                }
            } 
            else 
            {
                if (!$to || ($to && !$to->id) )
                {
                    header('HTTP/1.0 400');
                    echo 'Email address not found!';
                    exit;
                }
            }

            // Modify these settings in a production environment to point to a real mail server

            $mail = new PHPMailer;
            $mail->IsMAIL();
            //$mail->SMTPDebug = 3;                               // Enable verbose debug output

            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = Config::MAIL_HOST;  // Specify main and backup SMTP servers
            // $mail->SMTPAuth = true;                               // Enable SMTP authentication
            // $mail->Username = 'user@example.com';                 // SMTP username
            // $mail->Password = 'secret';                           // SMTP password
            // $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = Config::MAIL_PORT;                                    // TCP port to connect to

            $mail->setFrom(Config::SYSADMIN, 'Administrator');
            // send the email to one user or to many users
            if (is_array($to)){
                foreach ($to as $user) {
                    $mail->addAddress($user->email, $user->login);     // Add a recipient 
                }
            } else {
                $mail->addAddress($to->email, $to->login);     // Add a recipient
            }
            // $mail->addAddress('ellen@example.com');               // Name is optional
            // $mail->addReplyTo('info@example.com', 'Information');

            $mail->Subject = $subject;
            $mail->Body    = $body;

            if(!Config::MAIL_ON || !$mail->send()) {
                $context = Context::getinstance();
                $context->local()->addval('warnmessage', ["This application uses FakeMail (https://nilhcem.github.io/FakeSMTP/). You need to run this application for the mail to work, or connect to a real SMTP server via the module class/support/mailman.php sendmail function. Additional error: " . $mail->ErrorInfo]);
            }
            return 1;
        }
    }
 
?>