<?php
/**
 * A wrapper so that users dont need to edit the FWContext class in order to add features.
 *
 * @author Lindsay Marshall <lindsay.marshall@ncl.ac.uk>
 * @copyright 2016 Newcastle University
 *
 */
/**
 * A wrapper for the real Context class that allows people to extend it's functionality
 * in ways that are apporpriate for their particular website.
 */
    class Context extends FWContext
    {
/**
 * Do we have a logged in module leader user?
 *
 * @return boolean
 */
        public function hasmoduleleader()
        {
            return $this->hasuser() && $this->user()->ismoduleleader();
        }

/**
 * Do we have a logged in theme leader user?
 *
 * @return boolean
 */
        public function hasthemeleader()
        {
            return $this->hasuser() && $this->user()->isthemeleader();
        }

/**
 * Do we have a logged in supervisor leader user?
 *
 * @return boolean
 */
        public function hassupervisor()
        {
            return $this->hasuser() && $this->user()->issupervisor();
        }


/**
 * Check for an developer and 403 if not
 * @return void
 */
        public function mustbemoduleleader()
        {
            if (!$this->hasmoduleleader())
            {
                header('HTTP/1.0 403 Forbidden');
                echo 'Unauthorised!';
                exit;
            }
        }

/**
 * Check for a supervisor and 403 if not
 * @return void
 */
        public function mustbesupervisor()
        {
            if (!$this->hassupervisor())
            {
                header('HTTP/1.0 403 Forbidden');
                echo 'Unauthorised!';
                exit;
            }
        }

/**
 * Checks if the user has extra privileges, return 403 if not
 * @return void
 */
        
        public function mustbethemeormoduleleader()
        {
            if (!($this->hasthemeleader() or $this->hasmoduleleader()))
            {
                header('HTTP/1.0 403 Forbidden');
                echo 'Unauthorised!';
                exit;
            }
        }

/**
 * Returns 404 with a message
 * @param $message
 */
        public function httpnotfound($message = "Not found")
        {
            header('HTTP/1.0 404 Not-Found');
            echo $message;
            exit;
        }

/**
 * Returns 400 bad request
 * @param $message
 */
        public function httpbadrequest($message = "Malformed data")
        {
            header('HTTP/1.0 400 Bad Request');
            echo $message;
            exit;
        }

/**
 * Returns 403 with a message
 * @param $message
 */
        public function httpforbidden($message = "Forbidden")
        {
            header('HTTP/1.0 403 Forbidden');
            echo $message;
            exit;
        }
    }
?>
