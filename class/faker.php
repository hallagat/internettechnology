<?php
/**
 * Plamen Kolev <p.kolev@newcastle.ac.uk>
 * 2016-2017
 * A function that uses the framework https://github.com/fzaninotto/Faker to generate the whole website's sample data
 */
    $faker = Faker\Factory::create();

    // create fake users
    R::exec("delete from user where id != 1;");
    R::exec("delete from themechoice;");
    R::exec("delete from project;");
    R::exec("delete from theme;");
    R::exec('delete from page where name = "themes" or name = "administration" or name = "project" or name = "rolemanager";');
    R::exec('delete from rolename where name = "Supervisor" or name = "Themeleader" or name = "Moduleleader" or name = "Student";');
    
    // create pages
    $page1 = R::dispense("page");
    $page1->name = "themes";
    $page1->kind = 1;
    $page1->source = "Theme";
    $page1->active = 1;
    $page1->needlogin = 1;
    R::store($page1);

    $page1 = R::dispense("page");
    $page1->name = "rolemanager";
    $page1->kind = 1;
    $page1->source = "Rolemanager";
    $page1->active = 1;
    $page1->needlogin = 1;
    R::store($page1);


// create pages
    $projectpage = R::dispense("page");
    $projectpage->name = "project";
    $projectpage->kind = 1;
    $projectpage->source = "Project";
    $projectpage->active = 1;
    $projectpage->needlogin = 1;
    R::store($projectpage);

    $apipage = R::dispense("page");
    $apipage->name = "api";
    $apipage->kind = 1;
    $apipage->source = "Api";
    $apipage->active = 1;
    $apipage->needlogin = 1;
    R::store($apipage);

    $page2 = R::dispense("page");
    $page2->name = "administration";
    $page2->kind = 1;
    $page2->source = "Administration";
    $page2->active = 1;
    $page2->needlogin = 1;
    R::store($page2);

   // create user role names

    R::exec( 'INSERT into rolename (id, name) values(999, "Supervisor")');
    R::exec( 'INSERT into rolename (id, name) values(998, "Themeleader")');
    R::exec( 'INSERT into rolename (id, name) values(997, "Moduleleader")');
    R::exec( 'INSERT into rolename (id, name) values(996, "Student")');

    // // create special users

    $supervisor = R::dispense( 'user' );
    $supervisor->email = 'mikepaterson@newcastle.ac.uk';
    $supervisor->login = 'supervisor';
    $supervisor->password = '$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK';
    $supervisor->active = 1;
    $supervisor->confirm = 1;
    $supervisor->addrole('Site', 'Supervisor', '','2016-11-03 21:19:06');
    R::store( $supervisor );

    $supervisor = R::dispense( 'user' );
    $supervisor->email = 'janedoe@newcastle.ac.uk';
    $supervisor->login = 'themeleader';
    $supervisor->password = '$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK';
    $supervisor->active = 1;
    $supervisor->confirm = 1;
    $supervisor->addrole('Site', 'Themeleader', '','2016-11-03 21:19:06');
    R::store( $supervisor );

    $supervisor = R::dispense( 'user' );
    $supervisor->email = 'samvegan@newcastle.ac.uk';
    $supervisor->login = 'moduleleader';
    $supervisor->password = '$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK';
    $supervisor->active = 1;
    $supervisor->confirm = 1;
    $supervisor->addrole('Site', 'Moduleleader', '','2016-11-03 21:19:06');
    R::store( $supervisor );

    $supervisors = [];
    // create dummy theme_leaders
    foreach (range(0, 10) as $number)
    {
        $new_user = R::dispense( 'user' );
        $new_user->email = $faker->email;
        $new_user->login = 'themeleader' . $number;
        $new_user->password = '$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK';
        $new_user->active = 1;
        $new_user->confirm = 1;

        $new_user->addrole('Site', 'Themeleader', '','2016-11-03 21:19:06');
        R::store( $new_user );

        $supervisor = R::dispense( 'user' );
        $supervisor->email = "supervisor" . $number . "@website.com";
        $supervisor->login = 'supervisor' . $number;
        $supervisor->password = '$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK';
        $supervisor->active = 1;
        $supervisor->confirm = 1;

        $supervisor->addrole('Site', 'Supervisor', '','2016-11-03 21:19:06');
        R::store( $supervisor );
        $supervisors[$number] = $supervisor;
    }


    // create dummy users
    $students;
    foreach (range(0, 10) as $number)
    {
        $new_user = R::dispense( 'user' );
        $new_user->email = $faker->email;
        $new_user->login = 'student' . $number;
        $new_user->password = '$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK';
        $new_user->active = 1;
        $new_user->confirm = 1;
        $new_user->addrole('Site', 'Student', '','2016-11-03 21:19:06');
        R::store( $new_user );
        $students[$number] = $new_user;
    }



        // create test users
    
    foreach (range(0, 10) as $number)
    {
        $new_user = R::dispense( 'user' );
        $new_user->email = $faker->email;
        $new_user->login = 'testuser' . $number;
        $new_user->password = '$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK';
        $new_user->active = 1;
        $new_user->confirm = 1;
        $new_user->addrole('Site', 'Student', '','2016-11-03 21:19:06');
        R::store( $new_user );
    }



    // create fake themes

    $dummy_leader = R::dispense('user');
    $dummy_leader->email = 'dummy@dummy.com';
    $dummy_leader->login = 'dummy_themeleader1';
    $dummy_leader->password = '$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK';
    $dummy_leader->active = 1;
    $dummy_leader->confirm = 1;
    $dummy_leader->addrole('Site', 'Themeleader', '','2016-11-03 21:19:06');
    R::store( $dummy_leader );

    $theme = R::dispense( 'theme' );
    $theme->visible = 1;
    $theme->title = 'Dummy theme';
    $theme->description = "Dummy theme description";
    $theme->leader = $dummy_leader;
    R::store($theme);


    $themes;
    foreach (range(0, 10) as $number)
    {

        $theme = R::dispense( 'theme' );
        $theme->title = "Theme$number";
        $theme->description = $faker->paragraph;
        $theme->visible = 1;
        # get all users, pick a random one and use it's id as the owner_id
        
        $theme->leader = $dummy_leader;
        R::store( $theme );
        $themes[$number] = $theme;
    }

    // create fake projects
    foreach (range(0, 12) as $number)
    {
        $project = R::dispense( 'project' );
        $project->title = "Project" . $number;
        $project->visible = 1;
        $project->description = $faker->paragraph;
        $project->supervisor = $supervisors[$number % 2];
        # get all users, pick a random one and use it's id as the owner_id

        $project->theme = $themes[$number % 4];
        R::store( $project );
    }

    // create fake pending and confirmed theme choices
    foreach (range(0, 2) as $number){
        $themechoice = R::dispense('themechoice');
        $themechoice->confirmed = 0;
        $themechoice->user = $students[$number];
        $themechoice->first = $themes[$number];
        $themechoice->second = $themes[$number+1];
        R::store($themechoice);
    }

    $themechoice = R::dispense('themechoice');
    $themechoice->confirmed = 1;
    $themechoice->user = $students[4];
    $themechoice->first = $themes[2];
    $themechoice->second = $themes[3];
    R::store($themechoice);

    
?>

