<?php
/**
 * A model class for the RedBean object Project
 *
 * @author Plamen Kolev <p.kolev@newcastle.ac.uk>
 * @copyright 2016 Plamen Kolev
 *
 */

    class Model_Project extends RedBean_SimpleModel
    {
/**
 * Function to create a new project or edit an existing one, the id fetches it or is the pk for the theme
 * @param $id    Id of the theme that it will belong to
 * @return int         The operation was successful or not
 */
        public static function create_or_edit($id = -1, $action="create")
        {
            $errors=0;

            $context = Context::getinstance();
            $formdata = $context->formdata();
            $required_data = $formdata->haspost('title') && $formdata->haspost('description') && $formdata->haspost('supervisor') && $formdata->haspost('visible');
            if (!$required_data)
            {
                $context->httpbadrequest('Malformed data');
            }
            $title = $formdata->post('title');
            $description = $formdata->post('description');
            $supervisor_id = $formdata->post('supervisor');
            $visible = $formdata->post('visible');

            if (empty($title) || empty($description) || empty($supervisor_id))
            {
                $context->local()->message('errmessage', "Please fill all form fields!");
                ++$errors;
            }

            $visibility = 0;
            if ($visible !== "on" && $visible !== "off")
            {
                $context->local()->message('errmessage', "Please fill visibility form field!");
                ++$errors;
            } 
            else 
            {
                if ($visible === 'on')
                {
                    $visibility = 1;
                }
            }

            $supervisor = R::load('user', $supervisor_id);

            if (!$supervisor->id)
            {
                $context->local()->message('errmessage', "Unable to find supervisor for that project!");
                ++$errors;
            }

            if (!$errors) {
                if($action === "create")
                {
                    $project = R::dispense('project');
                }
                else if ($action === "edit") {
                    $project = R::load('project', $id);
                }
                else
                {
                    return 0;
                }

                $project->title = $title;
                $project->description = $description;
                $project->visible = $visibility;

                $project->supervisor = $supervisor;
                if($action === "create")
                {
                    $project->theme_id = $id;
                }
                // dd($project);
                R::store($project);
                return 1;
            }

            return 0;
        }


/**
 * Function to return the supervisor of this project
 * @param void
 * @return \RedBeanPHP\OODBBean
 */
        public function supervisor()
        {
            return R::load('user', $this->supervisor_id);
        }

    }
