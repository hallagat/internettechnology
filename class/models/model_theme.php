<?php
/**
 * A model class for the RedBean object Theme
 *
 * @author Plamen Kolev <p.kolev@newcastle.ac.uk>
 * @copyright 2016 Plamen Kolev
 *
 */

    class Model_Theme extends RedBean_SimpleModel
    {

/**
 * Helper function that returns all associated projects for a theme
 * @return array $array    Array of projects associated with a theme
 * @internal param $void
 *
 */

        public function projects()
        {
            return  R::findAll( 'project', 'theme_id=?', array($this->id) );
        }

/**
 * Returns the owner of the theme instance
 * @return mixed $object  ReadBean User object
 *
 * @internal param $void
 *
 */
        public function user()
        {
            return $this->user;
        }


/**
 * retrieves the owner of the theme
 * @return \RedBeanPHP\OODBBean
 */
        public function themeleader()
        {
            return R::load('user', $this->leader_id);
        }
/**
 * Function used to edit a theme, when this is done, it will send an email to the users of the system
 */
        public function edit()
        {
            $context = Context::getinstance();
            $errors = 0;
            $formdata = $context->formdata();

            if (!($formdata->haspost('title') && $formdata->haspost('description') && $formdata->haspost('visible')))
            {
                $context->local()->message('warnmessage', ["Please fill all form fields!"]);
                ++$errors;
                header('HTTP/1.0 400');
                echo 'Malformed request!';
                exit;
            }

            $title = $formdata->post('title');
            $description = $formdata->post('description');
            $visible = $formdata->post('visible');

            // if the visiility flag is neither on or off, error
            if ( ! ($visible === 'off' || $visible === 'on'))
            {
                $context->local()->addval('warnmessage', ["Visibility of the theme can only be on or off!"]);
                ++$errors;
            }

            if ( empty($title) )
            {

                $context->local()->message('errmessage', 'Title fields is required');
                ++$errors;
            }

            if (empty($description))
            {
                $context->local()->message('errmessage', 'Description fields is required');
                ++$errors;
            }

            if ($errors)
            {
                $context->local()->addval('theme', $this);
                ++$errors;
            }

            if (!$errors)
            {
                $visint = 0;
                if ($visible === 'on')
                {
                    $visint = 1;
                }

                $this->title = $title;
                $this->description = $description;
                $this->visible = $visint;

                R::store( $this );
                # if errors, return boolean false

                $students = Model_User::getstudents();
                Mailman::sendmail(
                    $students,
                    'Theme information for ' . $this->title . ' was just updated',
                    'Please visit the theme section for an overview of the changes'
                );
                header('Location: ' . Config::BASEDNAME . '/themes');
                exit;
            }

        }
    }
 ?>
