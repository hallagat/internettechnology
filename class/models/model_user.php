<?php
/**
 * A model class for the RedBean object User
 *
 * @author Lindsay Marshall <lindsay.marshall@ncl.ac.uk>
 * @copyright 2013-2014 Newcastle University
 *
 */
/**
 * A class implementing a RedBean model for User beans
 */
    class Model_User extends RedBean_SimpleModel
    {
/**
 * @var Array   Key is name of field and the array contains flags for checks
 */
        private static $editfields = array(
            'email'     => array(TRUE),         # array(NOTEMPTY)
        );
/**
 * Check for a role
 *
 * @param string        $contextname    The name of a context...
 * @param string	$rolename       The name of a role....
 *
 * @return object
 */
        public function hasrole($contextname, $rolename)
        {
            $cname = R::findOne('rolecontext', 'name=?', array($contextname));
            $rname = R::findOne('rolename', 'name=?', array($rolename));
            return R::findOne('role', 'rolecontext_id=? and rolename_id=? and user_id=? and start <= UTC_TIMESTAMP() and (end is NULL or end >= UTC_TIMESTAMP())',
                array($cname->getID(), $rname->getID(), $this->bean->getID()));
        }
/**
 * Check for a role
 *
 * @param string	$contextname    The name of a context...
 * @param string	$rolename       The name of a role....
 *
 * @return void
 */
        public function delrole($contextname, $rolename)
        {
            $cname = R::findOne('rolecontext', 'name=?', array($contextname));
            $rname = R::findOne('rolename', 'name=?', array($rolename));
            $bn = R::findOne('role', 'rolecontext_id=? and rolename_id=? and user_id=? and start <= UTC_TIMESTAMP() and (end is NULL or end >= UTC_TIMESTAMP())',
                array($cname->getID(), $rname->getID(), $this->bean->getID()));
            if (is_object($bn))
            {
                R::trash($bn);
            }
        }
/**
 *  Add a role
 *
 * @param string	$contextname    The name of a context...
 * @param string	$rolename       The name of a role....
 * @param string	$otherinfo      Any other info that is to be stored with the role
 * @param string	$start		A datetime
 * @param string	$end		A datetime or ''
 *
 * @return object
 */
        public function addrole($contextname, $rolename, $otherinfo, $start, $end = '')
        {
            $cname = R::findOne('rolecontext', 'name=?', array($contextname));
            if (!is_object($cname))
            {
                Web::getinstance()->bad();
            }
            $rname = R::findOne('rolename', 'name=?', array($rolename));
            if (!is_object($rname))
            {
                Web::getinstance()->bad();
            }
            $this->addrolebybean($cname, $rname, $otherinfo, $start, $end);
        }
/**
 *  Add a role
 *
 * @param object	$context        Contextname
 * @param object	$role           Rolename
 * @param string	$otherinfo      Any other info that is to be stored with the role
 * @param string	$start		A datetime
 * @param string	$end		A datetime or ''
 *
 * @return object
 */
        public function addrolebybean($context, $role, $otherinfo, $start, $end = '')
        {
            $r = R::dispense('role');
            $r->user = $this->bean;
            $r->rolecontext = $context;
            $r->rolename = $role;
            $r->otherinfo = $otherinfo;
            $r->start = $start;
            $r->end = $end === '' ? NULL : $end;
            R::store($r);
        }
/**
 * Get all currently valid roles for this user
 *
 * @param boolean	$all	If TRUE then include expired roles
 *
 * @return array
 */
        public function roles($all = FALSE)
        {
	    if ($all)
	    {
	        return $this->bean->with('order by start,end')->ownRole;
	    }
            return $this->bean->withCondition('start <= UTC_TIMESTAMP() and (end is null or end >= UTC_TIMESTAMP()) order by start, end')->ownRole;
        }
/**
 * Is this user an admin?
 *
 * @return boolean
 */
        public function isadmin()
        {
            return is_object($this->hasrole('Site', 'Admin'));
        }
/**
 * Is this user active?
 *
 * @return boolean
 */
        public function isactive()
        {
            return $this->bean->active;
        }
/**
 * Is this user confirmed?
 *
 * @return boolean
 */
        public function isconfirmed()
        {
            return $this->bean->confirm;
        }
/**
 * Is this user a developer?
 *
 * @return boolean
 */
        public function isdeveloper()
        {
            return is_object($this->hasrole('Site', 'Developer'));
        }
/**
 * Set the user's password
 *
 * @param string	$pw	The password
 *
 * @return void
 */
        public function setpw($pw)
        {
            $this->bean->password = password_hash($pw, PASSWORD_DEFAULT);
            R::store($this->bean);
        }
/**
 * Check a password
 *
 * @param string	$pw The password
 *
 * @return boolean
 */
        public function pwok($pw)
        {
            return password_verify($pw, $this->bean->password);
        }
/**
 * Set the email confirmation flag
 *
 * @return void
 */
        public function doconfirm()
        {
            $this->bean->active = 1;
            $this->bean->confirm = 1;
            R::store($this->bean);
        }
/**
 * Generate a token for this user that can be used as a unique id from a phone.
 *
 * @param string    $device     Currently not used!!
 *
 * @return string
 */
	public function maketoken($device = '')
	{
	    $token = (object)['iss' => Config::SITEURL, 'iat' => idate('U'), 'sub' => $this->bean->getID()];
	    return JWT::encode($token, Context::KEY);
	}
/**
 * Handle an edit form for this user
 *
 * @param object   $context    The context object
 *
 * @return void
 */
        public function edit($context)
        {
            $change = FALSE;
            $error = FALSE;
            $fdt = $context->formdata();
            foreach (self::$editfields as $fld => $flags)
            { // might need more fields for different applications
                $val = $fdt->post($fld, '');
                if ($flags[0] && $val === '')
                { // this is an error as this is a required field
                    $error = TRUE;
                }
                elseif ($val != $this->bean->$fld)
                {
                    $this->bean->$fld = $val;
                    $change = TRUE;
                }
            }
            if ($change)
            {
                R::store($this->bean);
            }
            $pw = $fdt->post('pw', '');
            if ($pw !== '')
            {
                if ($pw == $fdt->post('rpw', ''))
                {
                    $this->setpw($pw); // setting the password will do a store
                }
                else
                {
                    $error = TRUE;
                }
            }
            $uroles = $this->roles();
	    if ($fdt->haspost('exist'))
	    {
                foreach ($_POST['exist'] as $ix => $rid)
                {
                    $rl = $context->load('role', $rid);
                    $start = $_POST['xstart'][$ix];
                    $end = $_POST['xend'][$ix];
                    $other = $_POST['xotherinfo'][$ix];
                    if (strtolower($start) == 'now')
                    {
                        $rl->start = $context->utcnow();
                    }
                    elseif ($start != $rl->start)
                    {
                        $rl->start = $context->utcdate($start);
                    }
                    if (strtolower($end) == 'never' || $end === '')
                    {
                        if ($rl->end !== '')
                        {
                            $rl->end = NULL;
                        }
                    }
                    elseif ($end != $rl->end)
                    {
                         $rl->end = $context->utcdate($end);
                    }
                    if ($other != $rl->otherinfo)
                    {
                        $rl->otherinfo = $other;
                    }
                    R::store($rl);
                }
	    }
            foreach ($_POST['role'] as $ix => $rn)
            {
                $cn = $_POST['context'][$ix];
                if ($rn !== '' && $cn !== '')
                {
                    $end = $_POST['end'][$ix];
                    $start = $_POST['start'][$ix];
                    $this->addrolebybean($context->load('rolecontext', $cn), $context->load('rolename', $rn), $_POST['otherinfo'][$ix],
                        strtolower($start) == 'now' ? $context->utcnow() : $context->utcdate($start),
                        strtolower($end) == 'never' || $end === '' ? '' : $context->utcdate($end)
                    );
                }
            }
            return TRUE;
        }

/** ==================================== CUSTOM METHODS BELOW ============================**/


/**
 * Is this user a module leader?
 * @return bool $boolean
 */
        public function ismoduleleader()
        {
            return is_object($this->hasrole('Site', 'Moduleleader'));
        }

/**
 * Is this user a theme leader?
 * @return bool $boolean
 */
        public function isthemeleader()
        {
            return is_object($this->hasrole('Site', 'Themeleader'));
        }

/**
 * Is this user a supervisor?
 * @return bool $boolean
 */
        public function issupervisor()
        {
            return is_object($this->hasrole('Site', 'Supervisor'));
        }

/**
 * Get the confirmed theme of a student
 * @return array|string $string
 * @internal param $void
 *
 */
        public function theme()
        {

            if($this->canchosetheme())
            {
                return "No theme has been confirmed yet";
            }
            
            $rows = R::getRow( 'SELECT theme.* FROM themechoice INNER JOIN user, theme WHERE themechoice.first_id=theme.id AND confirmed=1 AND themechoice.user_id=user.id AND user_id=:user_id LIMIT 1;',
                [':user_id' => $this->id]
            );
            
            $theme = R::convertToBean('theme',$rows);
            return $theme;
        }

/**
 * Get supervisor associated with a user
 * @return \RedBeanPHP\OODBBean $object User
 * @internal param $void
 */

    public function supervisor()
    {
        # redbean sends a blank object (id=0 if no supervisor found)
        return R::findOne('user', 'id=?', array($this->supervisor_id));
    }

/**
 * Check if a user can choose a theme (already chosen or not a student)
 * @return bool $boolean  Tells if that user can choose a theme
 * @internal param $void
 *
 */
        public function canchosetheme()
        {

            $has_role = $this->hasrole('Site', 'Student');

            $has_made_choice = R::getRow( 'SELECT * FROM themechoice WHERE user_id = :user_id', 
                array(':user_id'=>$this->id) 
            );
            return $has_role && ! $has_made_choice;
        }

/**
 * Tells a student if their theme has been confirmed
 * @return int $boolean   Information about the user's choice (theme choice has been confirmed or not)
 * @internal param $void
 */

        public function themechoiceapproved()
        {
            if(!$this->hasrole('Site', 'Student'))
            {
                return 0;
            } 
            else {
                $themechoice = R::findOne('themechoice', 'user_id = ?' , array($this->id));
                if($themechoice->id && $themechoice->confirmed)
                {
                    return 1;
                } 
                else {
                    return 0;
                }
                
            }
        }

/**
 *  Returns all users who have confirmed themes from a module leader
 * @return array $array Users
 * @internal param $void
 *
 */
        public static function students_with_confirmed_themes()
        {
            $rows = R::getAll( 'SELECT user.* FROM user INNER JOIN themechoice where themechoice.confirmed=1 AND themechoice.user_id=user.id;');
            $students = R::convertToBeans('user',$rows);
            return $students;
        }

/**
 * Returns all supervisor user objects
 * @return array $array of User objects
 * @internal param $void
 *
 */
        public static function supervisors()
        {
            $rows = R::getAll('SELECT user.* FROM user INNER JOIN role, rolename where user.id=role.user_id AND rolename.name="Supervisor" AND role.rolename_id=rolename.id;');
            $supervisors = R::convertToBeans('user',$rows);
            return $supervisors;

        }
/**
 * Returns all module leader user objects
 * @return array $array of User objects
 * @internal param $void
 *
 */
        public static function themeleaders()
        {
            $rows = R::getAll('SELECT user.* FROM user INNER JOIN role, rolename where user.id=role.user_id AND rolename.name="Themeleader" AND role.rolename_id=rolename.id;');
            $supervisors = R::convertToBeans('user',$rows);
            return $supervisors;

        }

/**
 * Returns studentchoices associated with a particular theme leader
 * @return array $array    Themechoices choices associated with that theme leader
 * @internal param $void
 *
 */
        public function studentswithmytheme()
        {
            
            $rows = R::getAll('SELECT user.* FROM themechoice INNER JOIN theme, user where themechoice.user_id=user.id and themechoice.confirmed=1 and themechoice.first_id=theme.id and theme.leader_id=:leader_id;',
                array(':leader_id' => $this->id)
            );

            $mystudents = R::convertToBeans('user',$rows);
            return $mystudents;
        }

/**
 * Checks to see if a particular theme can be edited by the user
 * @param $theme
 * @return bool $bool     Wether the user owns the theme or not
 * @internal param The $themeint id of the theme you want to check ownership of
 *
 */

        public function ownstheme($theme)
        {
            return  ($this->id == $theme->leader_id) || $this->hasrole('Site', 'Moduleleader');
        }

/**
 * Returns all student objects
 * @return array $array    Array of Users that are students
 *
 * @internal param $void
 *
 */
        public static function getstudents()
        {
            $rows = R::getAll('
                SELECT user.* 
                FROM user 
                INNER JOIN role, rolename 
                WHERE role.user_id=user.id and rolename.id=role.rolename_id and rolename.name = "Student";'
            );

            $students = R::convertToBeans('user',$rows);
            return $students;
        }


/**
 * Returns students associated with the current supervisor
 * @return array $array    Array of Users that the user supervises, returns straight away if the user does not have a supervisor role
 *
 * @internal param $void
 *
 */
        public function supervisees()
        {
            $context = Context::getinstance()->mustbesupervisor();

            $rows = R::getAll('
                SELECT user.* 
                FROM user 
                WHERE supervisor_id=?
            ', array($this->id));

            $students = R::convertToBeans('user',$rows);
            return $students;
        }

        public function ownsproject($project)
        {
            if (
                $this->hasrole('Site', 'Moduleleader') || 
                $this->ownstheme($project->theme) || 
                $this->id == $project->supervisor_id
            ) 
            {
                return 1;
            }

            return 0;
        }

/**
 * Used to hide projects from students
 * @return bool  $bool wether the user can see the project or not
 * 
 */
        public function canviewproject($project)
        {
            if ($project->visible)
            {
                return 1;
            }
            
            if ($this->hasrole('Site', 'Student'))
            {
                return 0;
            }

            // if the logged in user is not a student, show the project
            return 1;
            
        }

/** 
 * Checks if a user is a supervisor and has assigned students, used when removing supervisors
 * @return bool does a supervisor have active students
 * 
 */
        public function supervises()
        {

            if (! $this->hasrole('Site', 'Supervisor'))
            {
                return false;
            }
            $rows = R::getAll('
                SELECT user.* 
                FROM user 
                WHERE supervisor_id=?
            ', array($this->id));
            if(sizeof($rows)){
                return true;
            }
            return false;
        }

    }
?>
