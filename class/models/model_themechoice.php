<?php
/**
 * A model class for the RedBean object Choice
 *
 * @author Plamen Kolev <p.kolev@newcastle.ac.uk>
 * @copyright 2016 Plamen Kolev
 *
 */
/**
 * A class implementing a RedBean model for Choice beans
 */
    class Model_Themechoice extends RedBean_SimpleModel
    {

/**
 * Return user associated with this theme choice
 * @return mixed $object
 * @internal param $void
 */
        public function user()
        {
            return $this->bean->user;
        }

/**
 * Return ThemeChoice object as the first preference
 * @return \RedBeanPHP\OODBBean $object
 * @internal param $void
 */
        public function first()
        {

            return R::load('theme', $this->bean->first_id);
        }


/**
 * Return ThemeChoice object as the second prerference
 * @return \RedBeanPHP\OODBBean $object
 */
        public function second()
        {
            return R::load('theme', $this->bean->second_id);
        }

/**
 * Creates a user theme choice object
 * @return int $bool   Wether the operation was successful or not
 * @internal param $void
 *
 */
        public function create()
        {
            $errors = 0;
            $context = Context::getinstance();
            $formdata = $fdt = $context->formdata();

            # check for critical failure, like no logged user or fields missing
            $data = ($formdata->haspost('first') && $formdata->haspost('second') && $context->user()->id);
            if (!$data)
            {
                header('HTTP/1.0 400');
                echo 'Malformed request!';
                exit;
            }
            $first = $formdata->post('first');
            $second = $formdata->post('second');


            if ($first === $second)
            {
                $context->local()->addval('warnmessage', ["Second preference must be a different theme choice !"]);
                ++$errors;
                return 0;
            }

            # validate (find the themes in the database)
            $theme1 = R::load('theme', $first);
            $theme2 = R::load('theme', $second);

            if (!$theme1->id or ! $theme2->id)
            {
                $context->local()->addval('warnmessage', ["We could not find some of your choices in the system, please try again !"]);
                ++$errors;
                return 0;
            }

            $this->user = $context->user();

            $this->first = $theme1;
            $this->second = $theme2;
            $this->confirmed = 0;

            R::store( $this );
            $context->local()->addval('warnmessage', ["Your choices have been submitted for approval"]);

            // do not check for logged in user, as this page has login require rules
            Mailman::sendmail(
                $context->user(),
                'First and second theme preferences have been submitted for approval',
                "Hello, " . $context->user()->login . ", the theme " . $theme1->title . " and ". $theme2->title ." have been submitted for the module leader's approval."
            );
            return 1;
        }

    }

?>
