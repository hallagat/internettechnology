#The system needs to
- Allow students to Browse projects by themes
- Allow students to choose a project within a theme
- Student should be able to choose a rank order of the themes they pick ( first, second choice )

- Supervisors and theme leaders must be able to upload information about topics within their themes,
- edit descriptions for the theme as a whole
- topics are available across several themes and can be hidden from students
- module leaders need to see list of students and the themes they have chosen ( sort by different criterias + filtering )
- after the module leaders have done the selection, theme leaders need to be able to see list of students that have selected their theme
- system should allow module leaders to allocate the students in their theme a supervisor
- system should automate email sending

# Story

## As a student, I want to
- View home page
- View a list of themes and projects associated with them, read their descriptions, read theme description
- choose theme choice 1 and 2
- choose a project ( 1 project per theme )

## As supervisor and theme leader, I want to
- I want to be able to upload new topics for my theme with description
- I want to be able to update the description of my themes
- I want to mark projects as not offered (hidden)

# As a module leader
- I want to be able to see a list of students and the themes they have picked with ordering and filtering
- I want to be able to associate a student with a theme

# As a theme leader
- I want to be able to see a list of students associated with my themes
- I want to be able to allocate the students taking my themes to a supervisor

# As the system, I want to sent event emails automatically

#Extrapolation

#The menu
- Home page
- theme list (contains tab-styled topics)
---
- (login/logout) or (welcome, logged user)
- (if logged as student, show entry "my choice")
- if logged in as a leader/supervisor you have (my themes/projects)
- if logged in as a leaders/supervisor you have (students taking my themes)
- if logged in as a leaders you have (assign students to projects)

# Questions:

Can theme leaders have multiple themes?
Can the students sign up (register) to the website, or they are predefined
<!-- Can we extend the user sql object? -->
Are the roles: Module leader, theme leaders, supervisors, students ?
Do we have named routes in the framework?
How to do redirects (header(Location:)) with a context
What exactly are the students picking? In the specification it is mentioned that they pick a theme preference 1 and 2, but nothing about the projects.
Are the themes already decided or the theme leader can create new ones?

Can the leader assign a student to a project of his/her choosing ?

Can we use boilerplate generated content or does the content have to look "proper"?
Can a theme leader edit all themes, or only the ones that he/she owns, does that apply to all other roles ?
How would the administrator privileges look like ?

# Feature checklist
DONE # 1. Only owner of theme can edit that theme
DONE # 2. module leader can edit any theme
3. sending emails when
 - new theme gets added, send email to students
 - student makes first second preference, email the theme leader
 - theme leader is assigned to a student, email both
 - moduleleader confirm student theme, email the student
4. User can pick a project
5. Moduleleader sees assign supervisor page
6. Supervisor can see mentees
7. Mark projects as not offered
8. Index page fixup
9. When a user is registered, associate that user as a student