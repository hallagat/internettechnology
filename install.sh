
# dependencies
#sudo apt-get install -yf mysql-workbench git

#git 
git config --global user.email "p.kolev22@gmail.com"
git config --global user.name "Plamen"

#pushd . && cd /tmp && git clone git@bitbucket.org:hallagat/internettechnology.git && mv internettechnology /opt/lampp/httdocs/webproject && popd

# setting up database
/opt/lampp/bin/mysql -u root -h localhost -e 'CREATE DATABASE webproject;'
/opt/lampp/bin/mysql -u root -h localhost -e 'CREATE DATABASE testdb;'
/opt/lampp/bin/mysql -u root -h localhost -e 'FLUSH PRIVILEGES;'
/opt/lampp/bin/mysql -u root -h localhost -e "CREATE USER 'admin'@'localhost' IDENTIFIED BY 'password';"
/opt/lampp/bin/mysql -u root -h localhost -e "GRANT ALL ON webproject.* TO admin@localhost IDENTIFIED BY 'password';"
/opt/lampp/bin/mysql -u root -h localhost -e "GRANT ALL ON testdb.* TO admin@localhost IDENTIFIED BY 'password';"
/opt/lampp/bin/mysql -u root -h localhost webproject -e "INSERT into page (name,kind,source,active, needlogin) values('themes',1,'Theme',1,1)";
/opt/lampp/bin/mysql -u root -h localhost webproject -e "INSERT into page (name,kind,source,active, needlogin) values('administration',1,'Administration',1,1)";

# create rolenames
/opt/lampp/bin/mysql -u root -h localhost webproject -e "INSERT into rolename (id, name) values(999, 'Supervisor')";
/opt/lampp/bin/mysql -u root -h localhost webproject -e "INSERT into rolename (id, name) values(998, 'Themeleader')";
/opt/lampp/bin/mysql -u root -h localhost webproject -e "INSERT into rolename (id, name) values(997, 'Moduleleader')";
/opt/lampp/bin/mysql -u root -h localhost webproject -e "INSERT into rolename (id, name) values(996, 'Student')";

# create different users for the roles (module,theme, supervisor)
/opt/lampp/bin/mysql -u root -h localhost webproject -e "INSERT into user (id, email, login, password, active, confirm) values(9999, 'mikepaterson@newcastle.ac.uk', 'supervisorman', '\$2y\$10\$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK', 1,1)";
/opt/lampp/bin/mysql -u root -h localhost webproject -e "INSERT into user (id, email, login, password, active, confirm) values(9998, 'janedoe@newcastle.ac.uk', 'themeleader', '\$2y\$10\$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK', 1,1)";
/opt/lampp/bin/mysql -u root -h localhost webproject -e "INSERT into user (id, email, login, password, active, confirm) values(9997, 'samvegan@newcastle.ac.uk', 'moduleleader', '\$2y\$10\$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK', 1,1)";

# assign these users to a role
/opt/lampp/bin/mysql -u root -h localhost webproject -e "INSERT into role (start, rolename_id, user_id, rolecontext_id) values('1970-01-01 00:00:00', 999, 9999,1)";
/opt/lampp/bin/mysql -u root -h localhost webproject -e "INSERT into role (start, rolename_id, user_id, rolecontext_id) values('1970-01-01 00:00:00', 998, 9998,1)";
/opt/lampp/bin/mysql -u root -h localhost webproject -e "INSERT into role (start, rolename_id, user_id, rolecontext_id) values('1970-01-01 00:00:00', 997, 9997,1)";
