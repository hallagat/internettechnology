<?php
    use \Codeception\Util\Locator as Locator;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
    class AcceptanceTester extends \Codeception\Actor
    {
        use _generated\AcceptanceTesterActions;
       
        public function loginas($username, $I, $password='password')
        {
            $I->seeInDatabase('user', array('login' => $username));
            $I->amOnPage('/logout');
            $I->amOnPage('/login');
            
            $I->fillField('login', $username);
            $I->fillField('password', $password);

            $I->click('#login_button');
            
            $I->seeInCurrentUrl('/');
            $I->see($username, Locator::find('li', ['id' => 'current_login_user_name']));
            $I->amOnPage('/themes');
            $I->see('Theme: Theme1');
            $I->dontSee('Login');
        }
    }
