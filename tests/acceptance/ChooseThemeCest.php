<?php
/**
 * Acceptance testing class that tests functions related to themes
 *
 * @author Plamen Kolev <p.kolev@newcastle.ac.uk>
 * @copyright 2016 Plamen Kolev
 *
 */
    use \Codeception\Util\Locator as Locator;

    class ChooseThemeCest
    {
        private $first_preference_title = "Theme1";
        private $second_preference_title = "Theme2";

        private $student = 'testuser1';
        private $moduleleader = 'moduleleader';

        private $password = 'password';

        public function _before(AcceptanceTester $I)
        {
            $I->loginas($this->student, $I);
            $I->amOnPage('/themes');

            $I->see($this->first_preference_title);

            $I->see($this->second_preference_title);

        }

        public function _after(AcceptanceTester $I)
        {
        }


/**
 * Tests if a user can successfully submit their themes
 * @param AcceptanceTester $I
 */
        public function submitThemePreferencesTest(AcceptanceTester $I)
        {
            $I->wantTo('As a student, I want to submit my first preference and second theme preference');
            $I->see("Choose a theme");
            $I->click('#choose_theme_link');
            $I->seeInCurrentUrl('/themes/choice');
            $I->see('First Choice');
            $I->selectOption('#first_choice',$this->first_preference_title);
            $I->selectOption('#second_choice',$this->second_preference_title);
            $I->click('Confirm choices');
            $I->see('Theme choice is still pending approval');
            $I->dontSee("Choose a theme");
            // now ensure that the post page is inaccessable
            $I->amOnPage('/themes/choice');
            $I->see('You have already selected a module themes or you are not a student');
            $I->dontSee('First choice');
            $I->see('Theme choice is still pending approval');
        }


/**
 * Ensures that once choice has been made, the form is no longer available
 * @param AcceptanceTester $I
 */
        public function submitSameThemeTwiceTest(AcceptanceTester $I)
        {
            $I->see("Choose a theme");
            $I->click('#choose_theme_link');
            $I->seeInCurrentUrl('/themes/choice');
            $I->see('First Choice');
            $I->selectOption('#first_choice',$this->first_preference_title);
            $I->selectOption('#second_choice',$this->first_preference_title);
            $I->click('Confirm choices');
            $I->see('Second preference must be a different theme choice !');
            $I->see("Choose a theme");
        }


/**
 * Tests that the module leader has the ability to submit a student choice as approved
 * @param AcceptanceTester $I
 */
        public function moduleLeaderConfirmsThemeChoiceTest(AcceptanceTester $I)
        {
            $I->click('#choose_theme_link');
            $I->selectOption('#first_choice',$this->first_preference_title);
            $I->selectOption('#second_choice',$this->second_preference_title);
            $I->click('Confirm choices');
            
            $I->see('Theme choice is still pending approval');

            $this->loginAsModuleLeader($I);
            $I->click('#student_themes_page');

            $first_choice = $I->grabTextFrom('//select[@name="choice"]/*[@selected]/text()');
            $I->assertContains('Theme0', $first_choice);
            
            $I->click('//tr[td/text()="'. $this->student .'"]/td/input[@type="submit"]');

            $I->seeElement('//tr[td/text()="' . $this->student .'"][@class="alert-success"]');
            $I->loginas($this->student, $I);
            $I->see('Theme approved');
        }


/**
 * Helper function
 * @param AcceptanceTester $I
 * @return int
 */
        public function loginAsModuleLeader(AcceptanceTester $I)
        {
            $I->amOnPage('/logout');
            $I->amOnPage('/login');
            $I->see("User Name");
            
            $I->fillField('login', $this->moduleleader);
            $I->fillField('password', $this->password);
            $I->click('#login_button');
            $I->dontSee('Login');
            $I->seeInCurrentUrl('/');
            $I->seeElement('#student_themes_page');
            return 1; 
        }
    }
