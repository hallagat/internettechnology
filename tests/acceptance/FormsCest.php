<?php
/**
 * Acceptance testing class that tests form validation
 *
 * @author Plamen Kolev <p.kolev@newcastle.ac.uk>
 * @copyright 2016 Plamen Kolev
 *
 */
    use \Codeception\Util\HttpCode;
    use \Codeception\Util\Locator as Locator;
    class FormsCest
    {

        public function _before(AcceptanceTester $I)
        {
            $this->first = $I->grabFromDatabase('theme', 'id', array('title' => 'Theme0'));
            $this->second = $I->grabFromDatabase('theme', 'id', array('title' => 'Theme1'));
            $this->student = 'student10';
            $this->userid = intval($I->grabFromDatabase('user', 'id', array('login' => $this->student)));
            $this->supervisorid = intval($I->grabFromDatabase('user', 'id', array('login' => 'supervisor')));

            $this->student2 = "student9";
            $this->student2id = intval($I->grabFromDatabase('user', 'id', array('login' => $this->student2)));
        }

        public function _after(AcceptanceTester $I)
        {
        }



/**
 * edit theme form tests where missing input should cause bad request
 * @param AcceptanceTester $I
 */
        public function editFormMissingInput(AcceptanceTester $I)
        {
            $I->loginas('dummy_themeleader1', $I);
            $I->amOnPage('/themes');
            $I->click('#edit_Theme0');
            $I->seeResponseCodeIs(HttpCode::OK);
            $url = $I->grabFromCurrentUrl();

            $I->sendPost($url, array('title' => '', 'description' => 'something'));
            $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);
            $I->see('Malformed request!');

            # obtain the ubdated entry, should be none as the data was invalid
            $entry = $I->grabFromDatabase('theme', 'id', array('title' => '', 'description' => 'something'));
            $I->comment($entry);
            $I->assertFalse($entry);
        }


/**
 * The trivial case
 * @param AcceptanceTester $I
 */
        public function editThemeFormTest(AcceptanceTester $I)
        {
            $I->loginas('dummy_themeleader1', $I);
            $I->amOnPage('/themes');
            $I->click('#edit_Theme0');
            $I->seeResponseCodeIs(HttpCode::OK);
            $url = $I->grabFromCurrentUrl();

            $I->sendPost($url, array('title' => '', 'description' => 'something', 'visible' => 'on'));
            $I->see('Title fields is required');
            $I->sendPost($url, array('title' => 'Valid', 'description' => '', 'visible' => 'on'));
            $I->see('Description fields is required');

            $entry = $I->grabFromDatabase('theme', 'id', array('title' => 'valid'));
            $I->comment($entry);
            $I->assertFalse($entry);
        }


/**
 * Tests if a theme can be hidden
 * @param AcceptanceTester $I
 */
        public function editThemeFormHideThemeTest(AcceptanceTester $I)
        {
            $I->loginas('dummy_themeleader1', $I);
            $I->amOnPage('/themes');
            $I->see('Theme0');
            $I->click('#edit_Theme0');
            $I->seeResponseCodeIs(HttpCode::OK);
            $url = $I->grabFromCurrentUrl();

            $I->sendPost($url, array('title' => 'foo', 'description' => 'something', 'visible' => 'off'));

            $I->amOnPage('/themes');
            $I->dontSee('Theme0');

            $entry = $I->grabFromDatabase('theme', 'id', array('title' => 'foo', 'visible' => 'off'));
            $I->comment($entry);
            $I->assertTrue(boolval($entry));
        }


/*
 * The user must be logged in to see choices
 * @param AcceptanceTester $I
 */
        public function submitThemeChoiceWithoutLoggedStudentTest(AcceptanceTester $I)
        {
            $I->comment("I am not logged in and will not be able to post");
            $I->sendPost('/themes/choice', array('first' => $this->first, 'second' => $this->second));
            $I->see('User Name');
        }


/**
 * Malformed form test
 * @param AcceptanceTester $I
 */
        public function submitThemeChoiceMissingInputFieldTest(AcceptanceTester $I)
        {

            $I->loginas($this->student, $I);
            $I->comment("Now i will miss one of the required fields");
            $I->sendPost('/themes/choice', array('first' => $this->first));

//            $user = intval($I->grabFromDatabase('user', 'id', array('login' => $this->student)));
            $entry = $I->grabFromDatabase('themechoice', 'id', array('user_id' => $this->userid));

            $I->comment($entry);
            $I->assertFalse(boolval($entry));

            $I->see('Malformed');
            $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);

            $I->comment("Now i will miss the other field");
            $I->sendPost('/themes/choice', array('second' => $this->second));
            $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);
            $I->see('Malformed');

//            $user = intval($I->grabFromDatabase('user', 'id', array('login' => 'student9')));
            $entry = $I->grabFromDatabase('themechoice', 'id', array('user_id' => $this->userid));

            $I->comment($entry);
            $I->assertFalse(boolval($entry));
        }

/**
 * The trivial case
 * @param AcceptanceTester $I
 */
        public function submitThemeChoiceStandardStudentTest(AcceptanceTester $I)
        {
            $I->loginas($this->student, $I);
            $I->comment("I am performing the trivial case");
            $I->sendPost('/themes/choice', array('first' => $this->first, 'second' => $this->second));

            $I->see('Alert! Theme choice is still pending approval');

//            $user = intval($I->grabFromDatabase('user', 'id', array('login' => $this->student)));
//            $I->comment($user);
            $entry = $I->grabFromDatabase('themechoice', 'id', array('user_id' => $this->userid, 'first_id' => intval($this->first), 'second_id' => intval($this->second)));
            $I->comment($entry);
            $I->assertTrue(boolval($entry));
        }

/**
 * This test is for extra sanity, just to make sure that the form is aware of the state of a student
* @param AcceptanceTester $I
*/
        public function studentSubmitsSameThemeTwice(AcceptanceTester $I)
        {
            $I->loginas($this->student, $I);
            $I->comment("I am performing the trivial case");
            $I->sendPost('/themes/choice', array('first' => $this->first, 'second' => $this->first));
            $I->dontSee('Alert! Theme choice is still pending approval');
            $I->see('Second preference must be a different theme choice');

//            $user = intval($I->grabFromDatabase('user', 'id', array('login' => $this->student)));
            $entry = $I->grabFromDatabase('themechoice', 'id', array('user_id' => $this->userid, 'first_id' => intval($this->first), 'second_id' => intval($this->first)));
            $I->assertFalse(boolval($entry));

        }

/**
 * By default, if a theme id does not correspond to a theme, the systeme should alert the user
 * @param AcceptanceTester $I
 */
        public function studentSubmitsNonexistingThemes(AcceptanceTester $I)
        {

            $I->loginas($this->student, $I);
            $I->comment("I am about to submit nonexisting theme choices");
            $I->sendPost('/themes/choice', array('first' => $this->first, 'second' => -111));
            $I->see('We could not find some of your choices in the system, please try again !');
            $I->amOnPage('/themes/choice');

//            $user = intval($I->grabFromDatabase('user', 'id', array('login' => $this->student)));
            $entry = $I->grabFromDatabase('themechoice', 'id', array('user_id' => $this->userid, 'first_id' => intval($this->first), 'second_id' => -111));
            $I->assertFalse(boolval($entry));
        }


/**
 *  confirm student to theme by a module leader where something goes wrong
 * @param AcceptanceTester $I
 */
        public function moduleleaderConfirmStudentToThemeMalformedTest(AcceptanceTester $I)
        {
            $I->loginas('moduleleader', $I);
//            $I->amOnPage('administration/theme');
            $I->comment("Will submit a user theme without userid, should cause malformed error");
            $I->sendPost('/administration/theme', array('choice' => $this->first));
            $I->see('Malformed');
            $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);

            $I->comment("Will submit a user theme without choice");
            $I->sendPost('/administration/theme', array('user_id' => $this->userid));
            $I->see('Malformed');
            $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);
        }

        /**
         * trivial case
         * @param AcceptanceTester $I
         */
        public function moduleLeaderConfirmsStudentTrivialTest(AcceptanceTester $I)
        {
            $I->loginas('moduleleader', $I);
            $I->haveInDatabase('themechoice', array('confirmed' => 0, 'user_id' => $this->userid, 'first_id'=> intval($this->first), 'second_id' => intval($this->second)));
            $I->sendPost('/administration/theme', array('user_id' => $this->userid, 'choice' => $this->second));
            $entry = $I->grabFromDatabase('themechoice', 'id', array('user_id' => $this->userid, 'first_id' => intval($this->second), 'confirmed' => 1));

            $I->assertTrue(boolval($entry));
        }

/**
 * Case where submiting a form with a confirmed student should not be able to happen
 * @param AcceptanceTester $I
 */
        public function moduleLeaderConfirmStudentTwiceTest(AcceptanceTester $I)
        {
            $I->loginas('moduleleader', $I);
            $I->haveInDatabase('themechoice', array('confirmed' => 0, 'user_id' => $this->userid, 'first_id'=> intval($this->first), 'second_id' => intval($this->second)));
            $I->sendPost('/administration/theme', array('user_id' => $this->userid, 'choice' => $this->second));
            $entry = $I->grabFromDatabase('themechoice', 'id', array('user_id' => $this->userid, 'first_id' => intval($this->second), 'confirmed' => 1));
            $I->assertTrue(boolval($entry));

            $I->comment("Try to update it again, but should not work");
            $I->sendPost('/administration/theme', array('user_id' => $this->userid, 'choice' => $this->first));
            $entry = $I->grabFromDatabase('themechoice', 'id', array('user_id' => $this->userid, 'first_id' => intval($this->first), 'confirmed' => 1));
            $I->comment("Entry should not be found");
            $I->assertFalse(boolval($entry));

        }

/**
 * More sanity methods
 * @param AcceptanceTester $I
 */
        public function moduleLeaderConfirmStudentNonexistentThemeTest(AcceptanceTester $I)
        {
            $I->loginas('moduleleader', $I);
            $I->haveInDatabase('themechoice', array('confirmed' => 0, 'user_id' => $this->userid, 'first_id'=> intval($this->first), 'second_id' => intval($this->second)));
            $I->sendPost('/administration/theme', array('user_id' => $this->userid, 'choice' => '-1'));
            $I->see('We could not find the user or the choices associated with him/her !');
        }

/**
 * confirming a nonexistant user
 * @param AcceptanceTester $I
 */
        public function moduleLeaderConfirmStudentNonexistentStudentTest(AcceptanceTester $I)
        {
            $I->loginas('moduleleader', $I);
            $I->haveInDatabase('themechoice', array('confirmed' => 0, 'user_id' => $this->userid, 'first_id'=> intval($this->first), 'second_id' => intval($this->second)));
            $I->sendPost('/administration/theme', array('user_id' => '-1', 'choice' => $this->first));
            $I->see('We could not find the user or the choices associated with him/her !');
        }

/**
 * assign student to supervisor
 * @param AcceptanceTester $I
 */
        public function assignSupervisorToStudentTrivialTest(AcceptanceTester $I)
        {
            $I->loginas('dummy_themeleader1', $I);
            $I->haveInDatabase('themechoice', array('confirmed' => 1, 'user_id' => $this->userid, 'first_id'=> intval($this->first), 'second_id' => intval($this->second)));

            $I->sendPOST('/administration/assign_supervisor', array('user_id' => $this->userid, 'supervisor' => $this->supervisorid));
            $I->dontSee('Malformed');
            $I->seeInDatabase('user', array('id' => $this->userid, 'supervisor_id' => $this->supervisorid));
        }

/**
 * tests when malicious form is posted
 * @param AcceptanceTester $I
 */
        public function assignSupervisorToStudentMalformedTest(AcceptanceTester $I)
        {
            $I->loginas('dummy_themeleader1', $I);
            $I->haveInDatabase('themechoice', array('confirmed' => 1, 'user_id' => $this->userid, 'first_id'=> intval($this->first), 'second_id' => intval($this->second)));

            $I->sendPOST('/administration/assign_supervisor', array('supervisor' => $this->supervisorid));
            $I->see('Malformed');
            $I->dontSeeInDatabase('user', array('id' => $this->userid, 'supervisor_id' => $this->supervisorid));

            $I->sendPOST('/administration/assign_supervisor', array('user_id' => $this->userid));
            $I->see('Malformed');
            $I->dontSeeInDatabase('user', array('id' => $this->userid, 'supervisor_id' => $this->supervisorid));

            $I->sendPOST('/administration/assign_supervisor', array('user_id' => '', 'supervisor' => ''));
            $I->see('We could not find the user or the supervisor associated with her/him !');
            $I->dontSeeInDatabase('user', array('id' => $this->userid, 'supervisor_id' => $this->supervisorid));
        }


/**
 * wrong studentid test
 * @param AcceptanceTester $I
 */
        public function assignSupervisorToStudentWrongStudentIdTest(AcceptanceTester $I)
        {
            $I->loginas('dummy_themeleader1', $I);
            $I->haveInDatabase('themechoice', array('confirmed' => 1, 'user_id' => $this->userid, 'first_id'=> intval($this->first), 'second_id' => intval($this->second)));

            $I->sendPOST('/administration/assign_supervisor', array('user_id' => '-1', 'supervisor' => $this->supervisorid));
            $I->dontSee('Malformed');
            $I->see('We could not find the user or the supervisor associated with her/him !');
            $I->dontSeeInDatabase('user', array('id' => -1, 'supervisor_id' => $this->supervisorid));
        }

/**
 * assign non-supervisor to student
 * @param AcceptanceTester $I
 */
        public function assignNonsupervisorToStudentTest(AcceptanceTester $I)
        {
            $I->loginas('dummy_themeleader1', $I);
            $I->haveInDatabase('themechoice', array('confirmed' => 1, 'user_id' => $this->userid, 'first_id'=> intval($this->first), 'second_id' => intval($this->second)));

            $I->sendPOST('/administration/assign_supervisor', array('user_id' => $this->userid, 'supervisor' => '-1'));
            $I->dontSee('Malformed');
            $I->see('We could not find the user or the supervisor associated with her/him !');
            $I->dontSeeInDatabase('user', array('id' => $this->userid, 'supervisor_id' => -1));
        }

/**
 * @param AcceptanceTester $I
 */
        public function assignInvalidSupervisorToStudentTest(AcceptanceTester $I)
        {
            $I->loginas('dummy_themeleader1', $I);
            $I->haveInDatabase('themechoice', array('confirmed' => 1, 'user_id' => $this->userid, 'first_id'=> intval($this->first), 'second_id' => intval($this->second)));

            $I->sendPOST('/administration/assign_supervisor', array('user_id' => $this->userid, 'supervisor' => $this->student2id));
            $I->dontSee('Malformed');
            $I->see('Invalid supervisor');
            $I->dontSeeInDatabase('user', array('id' => $this->userid, 'supervisor_id' => $this->student2id));
        }

    }
