<?php

use \Codeception\Util\Locator as Locator;
use \Codeception\Util\HttpCode;

    class UserAccessTestCest
    {

        public function _before(AcceptanceTester $I)
        {
            $this->projectid = 9999;
            $this->themeleader = "themeleader1";
            $this->themeleaderid = $I->grabFromDatabase('user', 'id', array('login' => $this->themeleader));
            $this->supervisor = 'supervisor1';
            $this->supervisorid = $I->grabFromDatabase('user', 'id', array('login' => $this->supervisor));
            $this->themeid = $I->haveInDatabase('theme', array('title' => 'Sample theme', 'description' => 'Sample description', "visible" => 1, "leader_id" => $this->themeleaderid));
            $I->haveInDatabase('project', array('id' => $this->projectid, 'title' => 'Dummy project', 'description' => 'Dummy description', 'theme_id' => $this->themeid, 'supervisor_id' => $this->supervisorid));
        }

        public function _after(AcceptanceTester $I)
        {
        }

        public function loginRequredTest(AcceptanceTester $I)
        {
            $I->amOnPage('/themes');
            $I->seeInCurrentUrl('/login');

            $I->amOnPage('/themes');
            $I->seeInCurrentUrl('/login');

            $I->amOnPage('administration/theme');
            $I->seeInCurrentUrl('/login');

            $I->amOnPage('/administration/assign_supervisor');
            $I->seeInCurrentUrl('/login');

            $I->amOnPage('/administration/supervisor_home');
            $I->seeInCurrentUrl('/login');
        }

        public function themePageVisibleByStudentsModuleSupervisorsLeadersTest(AcceptanceTester $I)
        {
            $I->loginas('student0', $I);
            $I->loginas('supervisor', $I);
            $I->loginas('moduleleader', $I);
            $I->loginas('themeleader', $I);
        }

        public function onlyModuleLeaderCanConfirmStudents(AcceptanceTester $I)
        {
            $I->loginas('student0', $I);
            $I->amOnPage('administration/theme');
            $I->seeResponseCodeIs(HttpCode::FORBIDDEN);

            $I->loginas('supervisor', $I);
            $I->amOnPage('administration/theme');
            $I->seeResponseCodeIs(HttpCode::FORBIDDEN);


            $I->loginas('themeleader', $I);
            $I->amOnPage('administration/theme');
            $I->seeResponseCodeIs(HttpCode::FORBIDDEN);

            $I->loginas('moduleleader', $I);
            $I->amOnPage('administration/theme');
            $I->seeResponseCodeIs(HttpCode::OK);
        }
        public function studentCantEditOrCreateProjectAndDoesNotSeeButtonsTest(AcceptanceTester $I)
        {
            $I->loginas('student1', $I);
            
            $I->dontSeeElement('#theme_' . $this->themeid . '_add_project_button');
            $I->dontSeeElement('#theme_' . $this->themeid . '_edit_project_'. $this->projectid .'_button');
            
            $I->amOnPage('/project/edit/' . $this->projectid);
            $I->seeResponseCodeIs(HttpCode::FORBIDDEN);

            $I->amOnPage('/project/create/' . $this->projectid);
            $I->seeResponseCodeIs(HttpCode::FORBIDDEN);
        }

        public function themeleaderNotOwningProjectCantEditAndCreateTest(AcceptanceTester $I)
        {
            $I->comment("This theme leader does not own the theme in which the project is no access should be given");
            $I->loginas('dummy_themeleader1', $I);

            $I->dontSeeElement('#theme_' . $this->themeid . '_add_project_button');
            $I->dontSeeElement('#theme_' . $this->themeid . '_edit_project_'. $this->projectid .'_button');

            $I->amOnPage('/project/edit/' . $this->projectid);
            $I->seeResponseCodeIs(HttpCode::FORBIDDEN);

            $I->amOnPage('/project/create/' . $this->projectid);
            $I->seeResponseCodeIs(HttpCode::FORBIDDEN);
        }

        public function supervisorNotOwningProjectCantEditAndCreateTest(AcceptanceTester $I)
        {
            $I->comment("This theme leader does not own the theme in which the project is no access should be given");
            $I->loginas('supervisor2', $I);

            $I->dontSeeElement('#theme_' . $this->themeid . '_add_project_button');
            $I->dontSeeElement('#theme_' . $this->themeid . '_edit_project_'. $this->projectid .'_button');

            $I->amOnPage('/project/edit/' . $this->projectid);
            $I->seeResponseCodeIs(HttpCode::FORBIDDEN);

            $I->amOnPage('/project/create/' . $this->projectid);
            $I->seeResponseCodeIs(HttpCode::FORBIDDEN);
        }

        public function themeleaderOwningProjectCreateAndEditTest(AcceptanceTester $I)
        {
            $I->comment("This theme leader owns the theme for the project, should be able to add and edit");
            $I->loginas($this->themeleader, $I);
            $I->seeElement('#theme_' . $this->themeid . '_add_project_button');
            $I->seeElement('#theme_' . $this->themeid . '_edit_project_'. $this->projectid .'_button');
            $I->see('Sample theme');
            
            $I->click('#theme_' . $this->themeid . '_add_project_button');
            $I->see('Supervisor for the project');
            $I->seeResponseCodeIs(HttpCode::OK);

            $I->amOnPage('/themes');
            $I->click('#theme_' . $this->themeid . '_edit_project_'. $this->projectid .'_button');
            $I->seeResponseCodeIs(HttpCode::OK);
        }

        public function supervisorOwningProjectCanEditTest(AcceptanceTester $I)
        {
            $I->loginas($this->supervisor, $I);
            $I->dontSeeElement('#theme_' . $this->themeid . '_add_project_button');
            $I->seeElement('#theme_' . $this->themeid . '_edit_project_'. $this->projectid .'_button');

            $I->amOnPage('/project/edit/' . $this->projectid);
            $I->seeResponseCodeIs(HttpCode::OK);

            $I->amOnPage('/project/create/' . $this->projectid);
            $I->seeResponseCodeIs(HttpCode::FORBIDDEN);
        }
    }
