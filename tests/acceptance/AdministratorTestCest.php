<?php
/**
 * Acceptance testing class that tests features associated with administrators
 *
 * @author Plamen Kolev <p.kolev@newcastle.ac.uk>
 * @copyright 2016 Plamen Kolev
 *
 */

    use \Codeception\Util\Locator as Locator;
    use \Codeception\Util\HttpCode;

    class AdministratorTestCest


    {

        private $themeleader = 'testthemeleader';

/**
 * Preconditions
 * @param AcceptanceTester $I
 */
        public function _before(AcceptanceTester $I)
        {
            $I->loginas('moduleleader', $I);
            $I->amOnPage('/administration/theme');
            $I->click('//tr[td/text()="student0"]/td/input[@type="submit"]');
            $I->click('//tr[td/text()="student1"]/td/input[@type="submit"]');
        }

        public function _after(AcceptanceTester $I)
        {
        }


/**
 * tests that only a theme leader can assign a supervisor, another theme leader would not be able to do so
 * @param AcceptanceTester $I
 */
        public function onlyLeaderOwningTheThemeCanAssignSupervisors(AcceptanceTester $I)
        {

            # login with a theme leader that does not own that theme
            $I->loginas('themeleader', $I);
            $I->amOnPage('/administration/assign_supervisor');
            $I->comment("I should not see users, as they did not join my module");
            $I->dontSee('student0');
            $I->dontSee('student1');
            $I->loginas('dummy_themeleader1', $I);
            $I->amOnPage('/administration/assign_supervisor');
            $I->comment("I see users, as they joined my module");
            $I->see('student0');
            $I->see('student1');

        }


/**
 * tests the broader case where a module leader has equal privileges to a theme owner
 * @param AcceptanceTester $I
 */
        public function onlyModuleLeaderAndThemeLeaderCanAssignSupervisors(AcceptanceTester $I)
        {
            $I->loginas('dummy_themeleader1', $I);
            $I->amOnPage('/administration/assign_supervisor');
            $I->see('student0');
            $I->see('student1');
            $I->click('//tr[td/text()="student0"]/td/input[@type="submit"]');

            $I->dontSee('//*[td/text()="student0"]/td[4]');

            $I->loginas('moduleleader', $I);
            $I->amOnPage('/administration/assign_supervisor');
            $I->dontSee('//*[td/text()="student0"]/td[4]');
            $I->click('//tr[td/text()="student1"]/td/input[@type="submit"]');
            $I->dontSee('//*[td/text()="student1"]/td[4]');

            $I->loginas('dummy_themeleader1', $I);
            $I->amOnPage('/administration/assign_supervisor');
            $I->dontSee('//*[td/text()="student1"]/td[4]');
        }


/**
 * edit theme as a non theme leader or supervisor
 * @param AcceptanceTester $I
 */
        public function editThemeWithoutBeingThemeLeaderOrModuleLeader(AcceptanceTester $I)
        {
            $I->loginas('dummy_themeleader1', $I);
            $I->amOnPage('/themes');
            $I->click('#edit_Theme0');
            $I->seeResponseCodeIs(HttpCode::OK);

            $url = $I->grabFromCurrentUrl();

            $I->loginas('themeleader', $I);
            $I->amOnPage('/themes');
            $I->dontSee('#edit_Theme0');
            $I->amOnPage($url);
            $I->seeResponseCodeIs(HttpCode::FORBIDDEN);
        }


/**
 *  edit theme as a non theme leader or supervisor
 * @param AcceptanceTester $I
 */
        public function editThemeAsModuleLeaderOrThemeLeader(AcceptanceTester $I)
        {
            $I->loginas('dummy_themeleader1', $I);
            $I->amOnPage('/themes');
            $I->click('#edit_Theme0');
            $I->seeResponseCodeIs(HttpCode::OK);

            $url = $I->grabFromCurrentUrl();
            $I->fillField('title', 'Updated');
            $I->click('Update');
            $I->see('Theme: Updated');

            $I->loginas('moduleleader', $I);
            $I->amOnPage('/themes');
            $I->click('#edit_Updated');
            $I->seeResponseCodeIs(HttpCode::OK);

            $I->fillField('title', 'Updated2');
            $I->click('Update');
            $I->see('Theme: Updated2');
        }
    }

