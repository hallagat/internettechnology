-- MySQL dump 10.16  Distrib 10.1.16-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: webproject
-- ------------------------------------------------------
-- Server version	10.1.16-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `confirm`
--

DROP TABLE IF EXISTS `confirm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `confirm` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `issued` datetime DEFAULT NULL,
  `kind` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_foreignkey_confirm_user` (`user_id`),
  CONSTRAINT `c_fk_confirm_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `confirm`
--

LOCK TABLES `confirm` WRITE;
/*!40000 ALTER TABLE `confirm` DISABLE KEYS */;
/*!40000 ALTER TABLE `confirm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fwconfig`
--

DROP TABLE IF EXISTS `fwconfig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fwconfig` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fwconfig`
--

LOCK TABLES `fwconfig` WRITE;
/*!40000 ALTER TABLE `fwconfig` DISABLE KEYS */;
INSERT INTO `fwconfig` VALUES (1,'sitename','nextstage'),(2,'siteurl','webproject.com'),(3,'sitenoreply','noreply@webproject.com'),(4,'sysadmin','admin@webproject.com'),(5,'bootcss','//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'),(6,'facss','//maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css'),(7,'jquery1','//code.jquery.com/jquery-1.12.4.min.js'),(8,'jquery2','//code.jquery.com/jquery-3.1.1.min.js'),(9,'bootjs','//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'),(10,'bootbox','//cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js'),(11,'parsley','https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.4.4/parsley.min.js');
/*!40000 ALTER TABLE `fwconfig` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page`
--

DROP TABLE IF EXISTS `page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kind` int(11) unsigned DEFAULT NULL,
  `source` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin` int(11) unsigned DEFAULT NULL,
  `needlogin` int(11) unsigned DEFAULT NULL,
  `devel` int(11) unsigned DEFAULT NULL,
  `mobileonly` int(11) unsigned DEFAULT NULL,
  `active` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=209 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page`
--

LOCK TABLES `page` WRITE;
/*!40000 ALTER TABLE `page` DISABLE KEYS */;
INSERT INTO `page` VALUES (1,'about',2,'about.twig',0,0,0,0,1),(2,'admin',1,'Admin',1,1,0,0,1),(3,'assets',1,'Assets',1,1,0,0,0),(4,'confirm',1,'UserLogin',0,0,0,0,1),(5,'contact',1,'Contact',0,0,0,0,1),(6,'devel',1,'Developer',1,1,1,0,1),(7,'forgot',1,'UserLogin',0,0,0,0,1),(8,'home',2,'index.twig',0,0,0,0,1),(9,'install.php',2,'oops.twig',0,0,0,0,1),(10,'login',1,'UserLogin',0,0,0,0,1),(11,'logout',1,'UserLogin',0,1,0,0,1),(12,'private',1,'GetFile',0,1,0,0,0),(13,'register',1,'UserLogin',0,0,0,0,1),(14,'upload',1,'Upload',0,0,0,0,0),(192,'api',1,'Api',NULL,1,NULL,NULL,1),(197,'api',1,'Api',NULL,1,NULL,NULL,1),(202,'api',1,'Api',NULL,1,NULL,NULL,1),(204,'themes',1,'Theme',NULL,1,NULL,NULL,1),(205,'rolemanager',1,'Rolemanager',NULL,1,NULL,NULL,1),(206,'project',1,'Project',NULL,1,NULL,NULL,1),(207,'api',1,'Api',NULL,1,NULL,NULL,1),(208,'administration',1,'Administration',NULL,1,NULL,NULL,1);
/*!40000 ALTER TABLE `page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visible` int(11) unsigned DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `theme_id` int(11) unsigned DEFAULT NULL,
  `supervisor_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_foreignkey_project_theme` (`theme_id`),
  KEY `index_foreignkey_project_supervisor` (`supervisor_id`),
  CONSTRAINT `c_fk_project_supervisor_id` FOREIGN KEY (`supervisor_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `c_fk_project_theme_id` FOREIGN KEY (`theme_id`) REFERENCES `theme` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `project` VALUES (14,'Project0',1,'In sed commodi consequatur. Distinctio molestiae quis labore. Quasi molestias natus dolor dolorem. Expedita dolore alias ut facilis.',457,1749),(15,'Project1',1,'Nam sunt ut voluptates. Error alias nemo natus accusantium sequi itaque enim ut. Commodi cumque nam sint.',458,1751),(16,'Project2',1,'Totam voluptatem est qui modi. Numquam cumque est vero eos qui. Facilis similique eum non blanditiis dolor sint.',459,1749),(17,'Project3',1,'Porro est totam maxime rerum. Numquam sit ut nostrum magni aut omnis rerum. Similique est in sint molestias.',460,1751),(18,'Project4',1,'Fuga ut ipsum et esse voluptates velit illo. Vero quasi beatae officiis voluptas.',457,1749),(19,'Project5',1,'Ut numquam sed ea eum cupiditate. Perspiciatis dolor nostrum quae maiores sed animi dolor. Deleniti consequatur eaque cumque consequatur omnis fuga. Provident accusamus soluta est qui.',458,1751),(20,'Project6',1,'Ea sapiente eos perferendis facilis id aspernatur pariatur. Aut et nesciunt possimus quia velit distinctio est. Incidunt aut quam laborum sed maiores vel. Error quisquam error neque similique pariatur iure ipsam amet.',459,1749),(21,'Project7',1,'Excepturi voluptas laborum aut doloremque. Et tempore ipsa aut impedit molestias qui. Ullam saepe voluptas sed perspiciatis.',460,1751),(22,'Project8',1,'Recusandae et sequi nisi. Commodi labore saepe nesciunt. Nihil beatae sit repudiandae soluta ipsum. Voluptas ut eos expedita asperiores maxime sed. Facilis suscipit culpa alias quasi.',457,1749),(23,'Project9',1,'Hic possimus accusamus ipsam. Ut totam eius veritatis iste dolores voluptas et similique. Modi optio molestiae sequi. Qui voluptatem fugiat ut perspiciatis.',458,1751),(24,'Project10',1,'Consequatur reprehenderit rerum quo ipsam. Distinctio aut deserunt excepturi ea. Earum harum dignissimos est molestiae voluptates deleniti incidunt. Assumenda quos distinctio ut ipsa atque voluptatem non dolorem.',459,1749),(25,'Project11',1,'Architecto cumque eveniet et et ullam. Quod rem pariatur ea iusto aliquid modi at voluptas. Libero minus in voluptatum vitae eos enim dolore.',460,1751),(26,'Project12',1,'Est aliquid assumenda ut sed laborum. Minus aut itaque eveniet. Optio delectus eum beatae voluptatem temporibus.',457,1749);
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `otherinfo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `rolename_id` int(11) unsigned DEFAULT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `rolecontext_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_foreignkey_role_rolename` (`rolename_id`),
  KEY `index_foreignkey_role_user` (`user_id`),
  KEY `index_foreignkey_role_rolecontext` (`rolecontext_id`),
  CONSTRAINT `c_fk_role_rolecontext_id` FOREIGN KEY (`rolecontext_id`) REFERENCES `rolecontext` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `c_fk_role_rolename_id` FOREIGN KEY (`rolename_id`) REFERENCES `rolename` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `c_fk_role_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1467 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'-','2016-10-16 18:42:28',NULL,1,1,1),(2,'-','2016-10-16 18:42:28',NULL,2,1,1),(1419,'','2016-11-03 21:19:06',NULL,999,1745,1),(1420,'','2016-11-03 21:19:06',NULL,998,1746,1),(1421,'','2016-11-03 21:19:06',NULL,997,1747,1),(1422,'','2016-11-03 21:19:06',NULL,998,1748,1),(1423,'','2016-11-03 21:19:06',NULL,999,1749,1),(1424,'','2016-11-03 21:19:06',NULL,998,1750,1),(1425,'','2016-11-03 21:19:06',NULL,999,1751,1),(1426,'','2016-11-03 21:19:06',NULL,998,1752,1),(1427,'','2016-11-03 21:19:06',NULL,999,1753,1),(1428,'','2016-11-03 21:19:06',NULL,998,1754,1),(1429,'','2016-11-03 21:19:06',NULL,999,1755,1),(1430,'','2016-11-03 21:19:06',NULL,998,1756,1),(1431,'','2016-11-03 21:19:06',NULL,999,1757,1),(1432,'','2016-11-03 21:19:06',NULL,998,1758,1),(1433,'','2016-11-03 21:19:06',NULL,999,1759,1),(1434,'','2016-11-03 21:19:06',NULL,998,1760,1),(1435,'','2016-11-03 21:19:06',NULL,999,1761,1),(1436,'','2016-11-03 21:19:06',NULL,998,1762,1),(1437,'','2016-11-03 21:19:06',NULL,999,1763,1),(1438,'','2016-11-03 21:19:06',NULL,998,1764,1),(1439,'','2016-11-03 21:19:06',NULL,999,1765,1),(1440,'','2016-11-03 21:19:06',NULL,998,1766,1),(1441,'','2016-11-03 21:19:06',NULL,999,1767,1),(1442,'','2016-11-03 21:19:06',NULL,998,1768,1),(1443,'','2016-11-03 21:19:06',NULL,999,1769,1),(1444,'','2016-11-03 21:19:06',NULL,996,1770,1),(1445,'','2016-11-03 21:19:06',NULL,996,1771,1),(1446,'','2016-11-03 21:19:06',NULL,996,1772,1),(1447,'','2016-11-03 21:19:06',NULL,996,1773,1),(1448,'','2016-11-03 21:19:06',NULL,996,1774,1),(1449,'','2016-11-03 21:19:06',NULL,996,1775,1),(1450,'','2016-11-03 21:19:06',NULL,996,1776,1),(1451,'','2016-11-03 21:19:06',NULL,996,1777,1),(1452,'','2016-11-03 21:19:06',NULL,996,1778,1),(1453,'','2016-11-03 21:19:06',NULL,996,1779,1),(1454,'','2016-11-03 21:19:06',NULL,996,1780,1),(1455,'','2016-11-03 21:19:06',NULL,996,1781,1),(1456,'','2016-11-03 21:19:06',NULL,996,1782,1),(1457,'','2016-11-03 21:19:06',NULL,996,1783,1),(1458,'','2016-11-03 21:19:06',NULL,996,1784,1),(1459,'','2016-11-03 21:19:06',NULL,996,1785,1),(1460,'','2016-11-03 21:19:06',NULL,996,1786,1),(1461,'','2016-11-03 21:19:06',NULL,996,1787,1),(1462,'','2016-11-03 21:19:06',NULL,996,1788,1),(1463,'','2016-11-03 21:19:06',NULL,996,1789,1),(1464,'','2016-11-03 21:19:06',NULL,996,1790,1),(1465,'','2016-11-03 21:19:06',NULL,996,1791,1),(1466,'','2016-11-03 21:19:06',NULL,998,1792,1);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rolecontext`
--

DROP TABLE IF EXISTS `rolecontext`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rolecontext` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fixed` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rolecontext`
--

LOCK TABLES `rolecontext` WRITE;
/*!40000 ALTER TABLE `rolecontext` DISABLE KEYS */;
INSERT INTO `rolecontext` VALUES (1,'Site',1);
/*!40000 ALTER TABLE `rolecontext` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rolename`
--

DROP TABLE IF EXISTS `rolename`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rolename` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fixed` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rolename`
--

LOCK TABLES `rolename` WRITE;
/*!40000 ALTER TABLE `rolename` DISABLE KEYS */;
INSERT INTO `rolename` VALUES (1,'Admin',1),(2,'Developer',NULL),(996,'Student',NULL),(997,'Moduleleader',NULL),(998,'Themeleader',NULL),(999,'Supervisor',NULL);
/*!40000 ALTER TABLE `rolename` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `theme`
--

DROP TABLE IF EXISTS `theme`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `theme` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `leader_id` int(11) unsigned DEFAULT NULL,
  `visible` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_foreignkey_theme_leader` (`leader_id`),
  CONSTRAINT `c_fk_theme_leader_id` FOREIGN KEY (`leader_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=468 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `theme`
--

LOCK TABLES `theme` WRITE;
/*!40000 ALTER TABLE `theme` DISABLE KEYS */;
INSERT INTO `theme` VALUES (456,'Dummy theme','Dummy theme description',1792,1),(457,'Theme0','Dignissimos omnis ratione nihil aut. Minima tempore est labore est.',1792,1),(458,'Theme1','Voluptas qui error et quis doloribus fugiat. Maiores sit minima quaerat accusantium necessitatibus. Dolores fuga repellat quia quibusdam facilis laudantium.',1792,1),(459,'Theme2','Est et magni tenetur voluptas optio enim. Facere quibusdam consectetur est natus laborum eveniet eius et. Ullam delectus consequatur necessitatibus aperiam asperiores adipisci. Adipisci voluptas expedita quibusdam quidem.',1792,1),(460,'Theme3','Expedita sunt ducimus ad sequi et consequatur omnis. Ut provident quasi repellat maxime repudiandae rerum quibusdam. Distinctio aliquid est consequatur asperiores. Magnam dolorem debitis minus saepe voluptatibus. Voluptatem ut quia similique eos ea.',1792,1),(461,'Theme4','Odio placeat assumenda magnam corporis harum. Iusto sapiente delectus est. Aspernatur est dolore libero nulla corrupti qui. Repellendus sint autem officia similique quia ducimus minus.',1792,1),(462,'Theme5','Natus inventore ipsam adipisci. Architecto dicta quia quas qui nulla.',1792,1),(463,'Theme6','Ipsum fugit aliquid et ratione dolores. Tenetur amet atque omnis ab repellendus ea voluptas. Omnis eos eius voluptas autem.',1792,1),(464,'Theme7','Est eum tenetur corporis officia autem consequatur provident cupiditate. Assumenda velit totam reprehenderit voluptatibus necessitatibus. Dolores sit labore omnis quam reiciendis voluptatibus. Ipsum iste unde inventore enim ducimus.',1792,1),(465,'Theme8','Omnis tempore et quasi eligendi impedit et. Sequi qui odit pariatur est amet qui velit ducimus. Velit assumenda molestiae repudiandae rerum. Cumque autem neque dolor.',1792,1),(466,'Theme9','Sint esse enim tempore aut est velit. Dignissimos magnam quo qui non nisi expedita. Magnam sit ut reprehenderit similique necessitatibus reprehenderit rerum.',1792,1),(467,'Theme10','Et rem accusamus aut iure ea porro nemo. Aut illum ratione nihil. Ut dolor deserunt aliquam rerum. At culpa voluptates error repellendus cum tempora.',1792,1);
/*!40000 ALTER TABLE `theme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `themechoice`
--

DROP TABLE IF EXISTS `themechoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `themechoice` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `confirmed` int(11) unsigned DEFAULT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `first_id` int(11) unsigned DEFAULT NULL,
  `second_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  UNIQUE KEY `user_id_2` (`user_id`),
  UNIQUE KEY `user_id_3` (`user_id`),
  UNIQUE KEY `user_id_4` (`user_id`),
  UNIQUE KEY `user_id_5` (`user_id`),
  UNIQUE KEY `user_id_6` (`user_id`),
  UNIQUE KEY `user_id_7` (`user_id`),
  UNIQUE KEY `user_id_8` (`user_id`),
  UNIQUE KEY `user_id_9` (`user_id`),
  KEY `index_foreignkey_themechoice_user` (`user_id`),
  KEY `index_foreignkey_themechoice_second` (`second_id`),
  CONSTRAINT `c_fk_themechoice_second_id` FOREIGN KEY (`second_id`) REFERENCES `theme` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `c_fk_themechoice_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `themechoice`
--

LOCK TABLES `themechoice` WRITE;
/*!40000 ALTER TABLE `themechoice` DISABLE KEYS */;
INSERT INTO `themechoice` VALUES (103,0,1770,457,458),(104,0,1771,458,459),(105,0,1772,459,460),(106,1,1774,459,460);
/*!40000 ALTER TABLE `themechoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) unsigned DEFAULT NULL,
  `confirm` int(11) unsigned DEFAULT NULL,
  `joined` datetime DEFAULT NULL,
  `supervisor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1793 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin@webproject.com','admin','$2y$10$JMUrQ9hRmYOt1X314oSr9.fnRrIR7Y8f6mmAr697WttLokBIhW8Cy',1,1,'2016-10-16 18:42:28',NULL),(1745,'mikepaterson@newcastle.ac.uk','supervisor','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1746,'janedoe@newcastle.ac.uk','themeleader','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1747,'samvegan@newcastle.ac.uk','moduleleader','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1748,'bertha.olson@gmail.com','themeleader0','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1749,'supervisor0@website.com','supervisor0','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1750,'zwalker@hotmail.com','themeleader1','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1751,'supervisor1@website.com','supervisor1','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1752,'hettinger.felton@yahoo.com','themeleader2','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1753,'supervisor2@website.com','supervisor2','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1754,'tbailey@klocko.net','themeleader3','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1755,'supervisor3@website.com','supervisor3','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1756,'crooks.julia@hotmail.com','themeleader4','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1757,'supervisor4@website.com','supervisor4','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1758,'wrowe@gmail.com','themeleader5','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1759,'supervisor5@website.com','supervisor5','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1760,'wilkinson.antonio@gmail.com','themeleader6','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1761,'supervisor6@website.com','supervisor6','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1762,'konopelski.bret@hotmail.com','themeleader7','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1763,'supervisor7@website.com','supervisor7','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1764,'lillian59@stoltenberg.net','themeleader8','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1765,'supervisor8@website.com','supervisor8','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1766,'sarah54@yahoo.com','themeleader9','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1767,'supervisor9@website.com','supervisor9','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1768,'alvis.littel@hotmail.com','themeleader10','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1769,'supervisor10@website.com','supervisor10','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1770,'mylene14@gmail.com','student0','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1771,'lelia84@littel.info','student1','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1772,'yost.hildegard@hotmail.com','student2','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1773,'ines.schaefer@yahoo.com','student3','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1774,'nikolaus.eldora@hotmail.com','student4','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1775,'iward@moen.com','student5','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1776,'ipouros@rice.com','student6','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1777,'darrick92@kulas.biz','student7','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1778,'weber.theresia@oconnell.net','student8','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1779,'camryn.mccullough@metz.org','student9','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1780,'owiza@yahoo.com','student10','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1781,'boehm.charlene@yahoo.com','testuser0','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1782,'sgreenholt@prohaska.com','testuser1','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1783,'zbuckridge@gmail.com','testuser2','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1784,'piper.rutherford@roob.biz','testuser3','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1785,'salvatore10@gmail.com','testuser4','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1786,'auer.kamryn@hotmail.com','testuser5','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1787,'bailey.crooks@gmail.com','testuser6','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1788,'janiya85@hotmail.com','testuser7','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1789,'hoppe.aisha@gmail.com','testuser8','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1790,'jerde.haylie@russel.org','testuser9','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1791,'abigail.cassin@kreiger.info','testuser10','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL),(1792,'dummy@dummy.com','dummy_themeleader1','$2y$10$pNC8SBmBv5ENIMB5tf5eWOUVecLUjLP2hYA10M6Gde4wHWs84xNoK',1,1,NULL,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-15 15:29:37
